#!/bin/sh
set -e

echo "Building for $1"

platform=${1:-$TARGET_PLATFORM}
optimize=${2:-${OPTIMIZE:-false}}
DEBUG=${DEBUG:-false}
extra=""
target=""

# rm ./zig-out/bin/* || true

if [ "$#" -gt 2 ]; then
  # Shift the first two arguments (1 and 2)
  shift 2
  extra="$@"
fi

# List of targets that support the optimization flag
support_optimization=$(
  cat <<EOF
aarch64-linux-musl
arm-linux-musl
x86_64-linux-musl
x86_64-windows-gnu
x86-windows-gnu
EOF
)

optimizable_binary=$(
  cat <<EOF
pvsm
pvsm.exe
pvsm-local
pvsm-local.exe
pvsm-cli
pvsm-cli.exe
pvsm-server
pvsm-server.exe
EOF
)

case "$platform" in
"linux/arm64"*)
  target="aarch64-linux-musl"
  ;;
"linux/arm"*)
  target="arm-linux-musl"
  ;;
"linux/amd64"*)
  target="x86_64-linux-musl"
  ;;
"linux/mips"*)
  target="mips-linux-musl"
  ;;
"linux/riscv64"*)
  target="riscv64-linux-musl"
  ;;
"windows/amd64"*)
  target="x86_64-windows-gnu"
  ;;
"windows/386"*)
  target="x86-windows-gnu"
  ;;
"windows/arm64"*)
  target="aarch64-windows-gnu"
  ;;
"windows/arm"*)
  target="arm-windows-gnu"
  ;;
"darwin/arm64"*)
  target="aarch64-macos-none"
  ;;
"darwin/amd64"*)
  target="x86_64-macos-none"
  ;;
"wasm"*)
  target="wasm32-freestanding"
  ;;
*)
  echo "Unknown machine type: $platform"
  exit 1
  ;;
esac

if [ "$DEBUG" == "true" ]; then
  zig build -Dtarget=$target --summary all $extra
else
  zig build \
  	-freference-trace \
  	-Dtarget=$target \
  	--release=small \
  	-Doptimize=ReleaseSmall \
   	--summary all $extra
fi

if echo "$support_optimization" | grep -q "$target" 2>&1; then
  optimizable=true
else
  optimizable=false
fi

if [ "$optimize" == "true" ] && [ "$optimizable" != "false" ]; then
  echo "Compressing binary with upx ..."
  for bin in zig-out/bin/*; do
    binary_name=$(basename $bin)
    if echo "$optimizable_binary" | grep -q "$binary_name" 2>&1; then
      upx -9 $bin || true
    else
      echo "can't be compressed: $bin"
    fi
  done
fi

if [ "$optimize" == "true" ] && [ "$optimizable" == "false" ]; then
  echo "Binary can't be compressed."
fi
