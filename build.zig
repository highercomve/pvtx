const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.

const BinaryOption = enum {
    all,
    cli,
    jsonsh,
    local,
    main,
    server,
    wasm,
    clib,
};

pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const options = b.addOptions();

    const binary_to_build = b.option(
        BinaryOption,
        "binary",
        "binary to be built.",
    ) orelse .main;

    options.addOption(
        ?BinaryOption,
        "binary",
        binary_to_build,
    );

    const exe = b.addExecutable(.{
        .name = "pvsm",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const local = b.addExecutable(.{
        .name = "pvsm-local",
        .root_source_file = b.path("src/local.zig"),
        .target = target,
        .optimize = optimize,
    });

    const jsonsh = b.addExecutable(.{
        .name = "jsonsh",
        .root_source_file = b.path("src/jsonsh.zig"),
        .target = target,
        .optimize = optimize,
    });

    const cli = b.addExecutable(.{
        .name = "pvsm-cli",
        .root_source_file = b.path("src/cli.zig"),
        .target = target,
        .optimize = optimize,
    });

    const server = b.addExecutable(.{
        .name = "pvsm-server",
        .root_source_file = b.path("src/server.zig"),
        .target = target,
        .optimize = optimize,
    });

    switch (binary_to_build) {
        .local => {
            b.installArtifact(local);
        },
        .cli => {
            b.installArtifact(cli);
        },
        .jsonsh => {
            b.installArtifact(jsonsh);
        },
        .main => {
            b.installArtifact(exe);
        },
        .server => {
            b.installArtifact(server);
        },
        .wasm => {
            const lib = b.addStaticLibrary(.{
                .name = "state_wasm",
                .root_source_file = b.path("src/pantavisor/state_wasm.zig"),
                .target = target,
                .optimize = optimize,
            });

            b.installArtifact(lib);
        },
        .clib => {
            const lib = b.addSharedLibrary(.{
                .name = "state",
                .root_source_file = b.path("src/pantavisor/state_lib.zig"),
                .target = target,
                .optimize = optimize,
            });

            lib.installHeader(b.path("src/pantavisor/state.h"), ".");

            b.installArtifact(lib);
        },
        .all => {
            b.installArtifact(local);
            b.installArtifact(cli);
            b.installArtifact(jsonsh);
            b.installArtifact(exe);
        },
    }

    // This *creates* a Run step in the build graph, to be executed when another
    // step is evaluated that depends on it. The next line below will establish
    // such a dependency.
    const run_cmd = b.addRunArtifact(exe);

    // By making the run step depend on the install step, it will be run from the
    // installation directory rather than directly from within the cache directory.
    // This is not necessary, however, if the application depends on other installed
    // files, this ensures they will be present and in the expected location.
    run_cmd.step.dependOn(b.getInstallStep());

    // This allows the user to pass arguments to the application in the build
    // command itself, like this: `zig build run -- arg1 arg2 etc`
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build run`
    // This will evaluate the `run` step rather than the default, which is "install".
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);

    // Similar to creating the run step earlier, this exposes a `test` step to
    // the `zig build --help` menu, providing a way for the user to request
    // running the unit tests.
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_exe_unit_tests.step);

    const check = b.step("check", "check if project compile");
    check.dependOn(&exe.step);
}
