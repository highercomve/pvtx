const std = @import("std");
const builtin = @import("builtin");
const mem = @import("std").mem;
const config = @import("config/config.zig");
const command = @import("cli/command.zig");

const server = @import("commands/serve.zig");

const cmd_server = server.init();

pub fn main() !void {
    const gpa_config = config.getGpaConfig();
    var gpa = std.heap.GeneralPurposeAllocator(gpa_config){};
    defer {
        if (builtin.mode == .Debug) {
            const check = gpa.deinit();
            const std_file = std.io.getStdErr();
            defer std_file.close();
            const stderr = std_file.writer();
            stderr.print("\n{any}\n", .{check}) catch {};
        } else {
            _ = gpa.deinit();
        }
    }
    const allocator = gpa.allocator();

    const std_file = std.io.getStdOut();
    const stdout = std_file.writer();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    var arguments = try allocator.alloc([:0]u8, args.len);
    defer {
        for (arguments) |s| {
            if (s.ptr != undefined) allocator.free(s);
        }
        allocator.free(arguments);
    }

    for (args, 0..) |s, i| {
        if (i == 0) {
            arguments[0] = try std.fmt.allocPrintZ(allocator, "serve", .{});
        } else {
            arguments[i] = try allocator.dupeZ(u8, s);
        }
    }

    try cmd_server.run(allocator, null, arguments, stdout);
}
