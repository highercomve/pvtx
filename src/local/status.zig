const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const status = @import("../transaction/status.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    trails: ?[]const u8 = "/storage/trails",
    revision: ?[]const u8 = "current",

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .revision = .r,
        .trails = .t,
    };

    pub const messages = .{
        .help = "how to use status",
        .pretty = "pretty format for json output",
        .revision = "revision ID to search for status",
        .trails = "trails folder path",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "status",
description: []const u8 = "show revision status",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    try status.exec(
        allocator,
        opt.args.trails.?,
        opt.args.revision.?,
        writer,
    );
}
