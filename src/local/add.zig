const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const add = @import("../transaction/add.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    objects: ?[]const u8,
    transaction: ?[]const u8 = "~/.pvsm/",

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .transaction = .t,
        .objects = .o,
    };

    pub const messages = .{
        .help = "how to use add",
        .pretty = "pretty format for json output",
        .transaction = "where is processing the transaction",
        .objects = "objects folder where is goint to put the objects unpacked",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "add",
description: []const u8 = "add package to current transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.args.objects == null) {
        try writer.print("add using pvcontrol is not implement yet.\nonly direct storage add is implmented.", .{});
        try opt.print_help(writer);
        return;
    }

    if (opt.positional_args.items.len == 0) {
        try writer.print(
            "adding package from standard input\n",
            .{},
        );
        try add.exec(
            allocator,
            opt.args.objects.?,
            opt.args.transaction.?,
            "-",
            writer,
        );
    }

    for (opt.positional_args.items) |package| {
        try writer.print(
            "adding package: {s}\n",
            .{package},
        );

        try add.exec(
            allocator,
            opt.args.objects.?,
            opt.args.transaction.?,
            package,
            writer,
        );
    }
}
