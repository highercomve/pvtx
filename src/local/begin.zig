const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const begin = @import("../transaction/begin.zig");
const models = @import("../pantavisor/models.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;
const abort = @import("../transaction/abort.zig");

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    source: []const u8,
    transaction: ?[]const u8 = "~/.pvsm/",

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .source = .s,
        .transaction = .t,
    };

    pub const messages = .{
        .help = "how to use add",
        .pretty = "pretty format for json output",
        .source = "pvr repo folder from where is going to start the transaction",
        .transaction = "folder where the transaction is going to be saved",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "begin",
description: []const u8 = "begin transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    _: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    const state = begin.exec(
        allocator,
        opt.args.source,
        opt.args.transaction.?,
    ) catch {
        return abort.exec(allocator, opt.args.transaction.?);
    };
    defer state.deinit();
}
