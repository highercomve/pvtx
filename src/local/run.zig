const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");

const run_rev = @import("../transaction/run.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    revision: []const u8,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .revision = .r,
    };

    pub const messages = .{
        .help = "how to use run",
        .pretty = "pretty format for json output",
        .revision = "revision to run",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "run",
description: []const u8 = "run revision using pvcontrol",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    try run_rev.exec(
        allocator,
        opt.args.revision,
        writer,
    );
}
