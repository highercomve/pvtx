const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const commit = @import("../transaction/commit.zig");
const abort = @import("../transaction/abort.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,
    transaction: ?[]const u8 = "~/.pvsm",
    objects_folder: ?[]const u8 = "/storage/objects",
    revision: ?[]const u8,

    pub const shorts = .{
        .help = .h,
        .transaction = .t,
        .revision = .d,
        .objects_folder = .o,
    };

    pub const messages = .{
        .help = "how to use deploy",
        .transaction = "path to transaction folder. default: ~/.pvsm/",
        .revision = "deploy folder",
        .objects_folder = "objects folder",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "commit",
description: []const u8 = "commit current transaction using pvcontrol",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    try commit.exec(
        allocator,
        opt.args.transaction.?,
        opt.args.revision.?,
        opt.args.objects_folder.?,
        writer,
    );

    try abort.exec(
        allocator,
        opt.args.transaction.?,
    );
}
