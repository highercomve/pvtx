const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const deploy = @import("../transaction/deploy.zig");
const abort = @import("../transaction/abort.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,
    transaction: ?[]const u8 = "~/.pvsm",
    deploy_folder: []const u8,
    objects_folder: []const u8,

    pub const shorts = .{
        .help = .h,
        .transaction = .t,
        .deploy_folder = .d,
        .objects_folder = .o,
    };

    pub const messages = .{
        .help = "how to use deploy",
        .transaction = "path to transaction folder. default: ~/.pvsm/",
        .deploy_folder = "deploy folder",
        .objects_folder = "objects folder",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "deploy",
description: []const u8 = "deploy current transaction directly to filesystem",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    try deploy.exec(
        allocator,
        opt.args.transaction.?,
        opt.args.deploy_folder,
        opt.args.objects_folder,
        writer,
    );

    try abort.exec(
        allocator,
        opt.args.transaction.?,
    );
}
