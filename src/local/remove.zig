const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");

const remove = @import("../transaction/remove.zig");
const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,
    transaction: ?[]const u8 = "~/.pvsm",

    pub const shorts = .{
        .help = .h,
        .transaction = .t,
    };

    pub const messages = .{
        .help = "how to use show",
        .transaction = "path to transaction folder. default: ~/.pvsm/",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "remove",
description: []const u8 = "remove a package from current transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "[remove part] <remove part> <remove part>...",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.positional_args.items.len == 0) {
        try opt.print_help(writer);
        return;
    }

    const json = try remove.exec(allocator, opt.args.transaction.?, opt.positional_args.items, writer);
    defer allocator.free(json);
}
