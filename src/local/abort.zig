const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const abort = @import("../transaction/abort.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    transaction: ?[]const u8 = "~/.pvsm/",

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .transaction = .t,
    };

    pub const messages = .{
        .help = "how to use add",
        .pretty = "pretty format for json output",
        .transaction = "folder where the transaction is going to be saved",
    };
};

// Command definiton
const Command = @This();

name: []const u8 = "abort",
description: []const u8 = "abort transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    try abort.exec(allocator, opt.args.transaction.?);

    try writer.print("transaction folder deleted! {s}\n", .{opt.args.transaction.?});
}
