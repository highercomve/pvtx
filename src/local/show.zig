const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const show = @import("../transaction/show.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,
    pretty: ?bool = true,
    transaction: ?[]const u8 = "~/.pvsm",

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .transaction = .t,
    };

    pub const messages = .{
        .help = "how to use show",
        .pretty = "pretty format for json output",
        .transaction = "path to transaction folder. default: ~/.pvsm/",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "show",
description: []const u8 = "show transaction json state",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    _: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    const stdfile = std.io.getStdOut();
    const stdout = stdfile.writer();

    try show.exec(allocator, opt.args.transaction.?, opt.args.pretty.?, stdout);
}
