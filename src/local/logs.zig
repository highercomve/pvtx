const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");

const logs = @import("../transaction/logs.zig");

const LocalArguments = @import("../commands/local.zig").LocalArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    path: ?[]const u8 = "/pantavisor/logs",
    revision: ?[]const u8 = "current",
    source: ?[]const u8,
    follow: ?bool = false,
    lines: ?usize = 10,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
        .revision = .r,
        .source = .s,
        .follow = .f,
        .lines = .n,
    };

    pub const messages = .{
        .help = "how to use run",
        .pretty = "pretty format for json output",
        .revision = "revision to run",
        .source = "source to be print",
        .follow = "follow file",
        .lines = "number of lines from the end",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "logs",
description: []const u8 = "print logs by revision and ",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &LocalArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const LocalArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    var sources = std.ArrayList([]const u8).init(allocator);
    defer sources.deinit();

    if (opt.args.source) |source| {
        var iterator = std.mem.split(
            u8,
            source,
            ",",
        );
        while (iterator.next()) |s| {
            try sources.append(s);
        }
    }

    try logs.exec(
        allocator,
        opt.args.path.?,
        opt.args.follow.?,
        opt.args.revision.?,
        opt.args.lines.?,
        sources,
        logs.Writer{ .fs = writer },
    );
}
