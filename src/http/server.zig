const std = @import("std");
const Allocator = @import("std").mem.Allocator;
const http = @import("std").http;
const mem = @import("std").mem;
const Request = http.Server.Request;
const Response = http.Server.Response;

const Thread = std.Thread;
const spawn = Thread.spawn;
const SpawnConfig = Thread.SpawnConfig;
const WaitGroup = Thread.WaitGroup;

const defaultNumberOfTheards: usize = 1;

const HandlerFn = *const fn (
    allocator: Allocator,
    request: *HttpRequest,
    response: *HttpResponse,
) anyerror!void;

const Headers = std.StringHashMap(http.Header);
const RouterList = std.StringHashMap(Route);

pub const QueryValues = std.ArrayList([]const u8);
pub const QueryMap = std.StringHashMap(QueryValues);
pub const LogLevel = enum(u10) {
    err = 10,
    warning = 20,
    info = 30,
    debug = 40,
};

fn hash_map_key(
    allocator: Allocator,
    method: std.http.Method,
    path: []const u8,
) ![]const u8 {
    return std.fmt.allocPrint(
        allocator,
        "{d}-{s}",
        .{ method, path },
    );
}

pub const RespondOptions = struct {
    version: http.Version = .@"HTTP/1.1",
    status: http.Status = .ok,
    reason: ?[]const u8 = null,
    keep_alive: bool = true,
    extra_headers: ?[]const http.Header = null,
    transfer_encoding: ?Response.TransferEncoding = null,
};

pub const HttpResponse = struct {
    allocator: Allocator,
    response: *Response,
    status: http.Status = .ok,
    version: http.Version = .@"HTTP/1.1",
    keep_alive: bool = true,
    headers: ?[]const http.Header = null,
    content: ?[]u8 = null,
    transfer_encoding: ?http.TransferEncoding = null,

    pub fn init(
        allocator: Allocator,
        request: *Request,
        buffer: []u8,
    ) !HttpResponse {
        var res = try allocator.create(Response);
        res.stream = request.server.connection.stream;
        res.send_buffer = buffer;
        res.send_buffer_start = 0;
        res.send_buffer_end = 0;
        res.transfer_encoding = .none;
        res.elide_body = false;
        res.chunk_len = 0;

        return HttpResponse{
            .allocator = allocator,
            .response = res,
        };
    }

    pub fn deinit(self: *HttpResponse) void {
        if (self.content) |content| {
            self.allocator.free(content);
            self.content = null;
        }
        // Free other dynamically allocated fields if any
        if (self.headers) |headers| {
            self.allocator.free(headers);
            self.headers = null;
        }

        self.allocator.destroy(self.response);
        self.* = undefined;
    }

    pub fn stream_closed(self: *HttpResponse) bool {
        return isStreamClosed(self.response.stream);
    }

    pub fn setContent(self: *HttpResponse, new_content: ?[]const u8) !void {
        if (self.content) |old_content| {
            self.allocator.free(old_content);
        }
        if (new_content) |content| {
            self.content = try self.allocator.alloc(u8, content.len);
            std.mem.copyForwards(u8, self.content.?, content);
        } else {
            self.content = null;
        }
    }

    pub fn getContent(self: *HttpResponse) ?[]const u8 {
        return if (self.content) |content| content else null;
    }

    pub fn respond(self: *HttpResponse, content: []const u8) !void {
        try self.setHeaders(.{
            .transfer_encoding = .{ .content_length = content.len },
        });
        try self.response.writeAll(content);
        try self.response.end();
    }

    pub fn setHeaders(
        self: *HttpResponse,
        options: RespondOptions,
    ) !void {
        var headers = std.ArrayList(http.Header).init(self.allocator);
        defer headers.deinit();

        if (self.headers == null and options.extra_headers == null) {
            self.headers = &.{
                .{ .name = "Content-Type", .value = "application/json" },
            };
        }
        if (self.headers) |saved_headers| {
            for (saved_headers) |header| {
                try headers.append(header);
            }
        }
        if (options.extra_headers) |extra_headers| {
            for (extra_headers) |header| {
                try headers.append(header);
            }
        }

        self.headers = try headers.toOwnedSlice();

        if (options.transfer_encoding) |te| {
            self.response.transfer_encoding = te;
        }

        var h = std.ArrayListUnmanaged(u8).initBuffer(self.response.send_buffer);

        h.fixedWriter().print(
            "{s} {d} {s}\r\n",
            .{
                @tagName(self.version),
                @intFromEnum(self.status),
                self.status.phrase() orelse "",
            },
        ) catch unreachable;

        switch (self.version) {
            .@"HTTP/1.0" => if (self.keep_alive) h.appendSliceAssumeCapacity("connection: keep-alive\r\n"),
            .@"HTTP/1.1" => if (!self.keep_alive) h.appendSliceAssumeCapacity("connection: close\r\n"),
        }

        switch (self.response.transfer_encoding) {
            .chunked => h.appendSliceAssumeCapacity("transfer-encoding: chunked\r\n"),
            .content_length => |len| {
                h.fixedWriter().print("content-length: {d}\r\n", .{len}) catch unreachable;
            },
            else => {},
        }

        for (self.headers.?) |header| {
            std.debug.assert(header.name.len != 0);
            h.appendSliceAssumeCapacity(header.name);
            h.appendSliceAssumeCapacity(": ");
            h.appendSliceAssumeCapacity(header.value);
            h.appendSliceAssumeCapacity("\r\n");
        }

        h.appendSliceAssumeCapacity("\r\n");
        self.response.send_buffer_end = h.items.len;
    }
};

pub const HttpRequest = struct {
    headers: Headers,
    request: *Request,
    a: Allocator,
    uri: std.Uri,

    pub fn init(a: Allocator, request: *Request, uri: std.Uri) !HttpRequest {
        var headerInterator = request.*.iterateHeaders();
        var r = HttpRequest{
            .a = a,
            .headers = Headers.init(a),
            .request = request,
            .uri = uri,
        };
        while (headerInterator.next()) |header| {
            try r.headers.put(header.name, header);
        }

        return r;
    }

    pub fn deinit(self: *HttpRequest) void {
        self.headers.deinit();
        self.* = undefined;
    }

    pub fn get_header(self: *HttpRequest, name: []const u8) ?http.Header {
        return self.headers.get(name);
    }

    pub fn get_query(self: *HttpRequest) !QueryMap {
        var queries = QueryMap.init(self.a);
        if (self.uri.query == null) {
            return queries;
        }

        const q = self.uri.query.?;
        const query = q.percent_encoded;

        var queries_iterator = std.mem.split(u8, query, "&");
        while (queries_iterator.next()) |query_string| {
            var key_values = std.mem.split(u8, query_string, "=");
            const key = key_values.first();
            var values_array = QueryValues.init(self.a);
            while (key_values.next()) |values| {
                var values_interator = std.mem.split(u8, values, ",");
                while (values_interator.next()) |v| {
                    try values_array.append(v);
                }
            }
            try queries.put(key, values_array);
        }

        return queries;
    }
};

pub const Route = struct {
    path: []const u8,
    method: http.Method,
    handler: HandlerFn,

    pub fn init(path: []const u8, method: http.Method, cb: HandlerFn) Route {
        return Route{
            .path = path,
            .method = method,
            .handler = cb,
        };
    }

    pub fn get_key(self: *Route, allocator: Allocator) ![]const u8 {
        return hash_map_key(allocator, self.method, self.path);
    }

    pub fn deinit(self: *Route, _: Allocator) void {
        // allocator.free(self.path);
        self.* = undefined;
    }
};

pub const Router = struct {
    allocator: Allocator,
    server: std.net.Server,
    ip: []const u8,
    port: u16,
    routes: RouterList,
    thread_count: usize = 1,
    log_level: LogLevel = .debug,
    log_writer: std.fs.File.Writer,
    log_file: std.fs.File,
    http_server: ?*http.Server = null,
    hostname: []const u8,

    pub fn init(
        allocator: Allocator,
        ip: []const u8,
        port: u16,
        log_level: LogLevel,
        thread_count: usize,
    ) !Router {
        const address = try std.net.Address.parseIp(ip, port);
        const server = try address.listen(.{ .reuse_address = true });
        const hostname = try std.fmt.allocPrint(
            allocator,
            "http://{s}:{d}",
            .{ ip, port },
        );

        var t: usize = thread_count;
        if (thread_count <= 0) {
            t = std.Thread.getCpuCount() catch defaultNumberOfTheards;

            if (t > defaultNumberOfTheards) {
                t = t - 1;
            }
        }

        var log_file = std.io.getStdOut();
        return Router{
            .allocator = allocator,
            .routes = RouterList.init(allocator),
            .ip = ip,
            .port = port,
            .server = server,
            .log_level = log_level,
            .log_file = log_file,
            .log_writer = log_file.writer(),
            .thread_count = t,
            .hostname = hostname,
        };
    }

    pub fn deinit(self: *Router) void {
        var iterator = self.routes.iterator();
        while (iterator.next()) |route| {
            self.allocator.free(route.key_ptr.*);
            route.value_ptr.deinit(self.allocator);
        }
        self.server.deinit();
        self.routes.deinit();
        self.allocator.free(self.hostname);
        self.log_file.close();
        self.* = undefined;
    }

    pub fn add_route(
        self: *Router,
        method: http.Method,
        path: []const u8,
        cb: HandlerFn,
    ) !void {
        const route = Route.init(path, method, cb);
        const key = try std.fmt.allocPrint(
            self.allocator,
            "{d}-{s}",
            .{ method, path },
        );
        try self.routes.put(key, route);
    }

    fn log(self: *Router, level: LogLevel, comptime fmt: []const u8, args: anytype) !void {
        if (@intFromEnum(self.log_level) < @intFromEnum(level)) {
            return;
        }

        const now = std.time.timestamp();
        const message = try std.fmt.allocPrint(self.allocator, fmt, args);
        defer self.allocator.free(message);

        try self.log_writer.print("{}: [{d}] -- {s}", .{ level, now, message });
    }

    pub fn concurrent_listen(self: *Router) !void {
        var wg = Thread.WaitGroup{};
        wg.reset();

        var threads = try self.allocator.alloc(Thread, self.thread_count);
        defer self.allocator.free(threads);

        for (threads) |*t| {
            wg.start();
            t.* = try spawn(
                .{},
                worker,
                .{
                    &wg, self.*,
                },
            );
        }

        wg.wait();
        try self.log(.debug, "All threads have started, waiting for completion\n", .{});

        for (threads[0..], 0..self.thread_count) |*t, index| {
            t.join();
            try self.log(.debug, "Joined thread {}\n", .{index});
        }
    }

    pub fn single_listen(self: *Router) !void {
        try self.log(.debug, "starting single theard server\n", .{});

        var send_buffer: [4096]u8 = undefined;
        var read_buffer: [4096]u8 = undefined;
        while (true) {
            self.process(&send_buffer, &read_buffer) catch |err| {
                std.debug.print("ERROR ON self.process -- {any}\n", .{err});
                continue;
            };
            // break;
        }
    }

    pub fn listen(self: *Router) !void {
        try self.log(.info, "listing at: {s}:{d}\n", .{ self.ip, self.port });

        var routes_iterator = self.routes.iterator();
        while (routes_iterator.next()) |route| {
            std.debug.print(
                "Route register {s} {any} {s}\n",
                .{ route.key_ptr.*, route.value_ptr.method, route.value_ptr.path },
            );
        }

        if (self.thread_count > 1) {
            try self.concurrent_listen();
        } else {
            try self.single_listen();
        }
    }

    pub fn process(self: *Router, res_headers_buffer: []u8, read_header_buffer: []u8) !void {
        const connection = try self.server.accept();
        defer connection.stream.close();

        const start = std.time.microTimestamp();
        var server = http.Server.init(
            connection,
            read_header_buffer,
        );

        while (server.state == .ready) {
            var request = try server.receiveHead();
            const url = try std.fmt.allocPrint(
                self.allocator,
                "{s}{s}",
                .{ self.hostname, request.head.target },
            );
            defer self.allocator.free(url);

            const uri = std.Uri.parse(url) catch |err| {
                const r = try std.fmt.allocPrint(self.allocator, "{}", .{err});
                defer self.allocator.free(r);

                try request.respond(r, .{});
                return err;
            };

            self.log(
                .info,
                "{}\t{s}\n",
                .{ request.head.method, request.head.target },
            ) catch {};

            if (request.head.content_type) |ct| {
                self.log(
                    .debug,
                    "request content size {s}\n",
                    .{ct},
                ) catch {};
            }

            var key = try hash_map_key(
                self.allocator,
                request.head.method,
                uri.path.percent_encoded,
            );
            std.debug.print("searching for route {s}\n", .{key});

            var r = self.routes.get(key);
            if (r == null) {
                self.allocator.free(key);
                key = try hash_map_key(
                    self.allocator,
                    request.head.method,
                    "*",
                );
                std.debug.print("searching for route {s}\n", .{key});

                r = self.routes.get(key);
            }
            defer self.allocator.free(key);

            if (r) |route| {
                var http_request = try HttpRequest.init(
                    self.allocator,
                    &request,
                    uri,
                );
                defer http_request.deinit();

                var http_response = HttpResponse.init(
                    self.allocator,
                    &request,
                    res_headers_buffer,
                ) catch |err| {
                    self.log(.debug, "ERROR HttpResponse.init: {any}\n", .{err}) catch {};
                    request.respond(
                        "{ \"error\": \"can't create http response\"}",
                        .{ .status = .internal_server_error },
                    ) catch {};
                    return err;
                };
                defer http_response.deinit();

                // make an arena allocator of all the allocations
                // that happens for every request, in that way we can
                // free everything after answering the request
                // without that much problem
                var arena = std.heap.ArenaAllocator.init(self.allocator);
                defer arena.deinit();

                // the arena allocator is the one that will
                // be pass to the route handler, therefore everything is
                // going to be free at the end of the request.
                const allocator = arena.allocator();

                route.handler(
                    allocator,
                    &http_request,
                    &http_response,
                ) catch |err| {
                    self.log(.debug, "ERROR route.handler: {any}\n", .{err}) catch {};
                    request.respond(
                        "{ \"error\": \"route can't be handle\"}",
                        .{ .status = .internal_server_error },
                    ) catch {};
                    return err;
                };

                if (http_response.content) |content| {
                    http_response.respond(content) catch |err| {
                        self.log(.debug, "ERROR http_response.respond: {any}\n", .{err}) catch {};
                        return err;
                    };
                } else {
                    http_response.response.end() catch |err| {
                        self.log(.debug, "ERROR http_response.respond.end: {any}\n", .{err}) catch {};
                        return err;
                    };
                }
            } else {
                request.respond(
                    "not found",
                    .{ .status = .not_found },
                ) catch |err| {
                    try self.log(.debug, "ERROR: {any}\n", .{err});
                    return err;
                };
            }

            const finish = std.time.microTimestamp();
            return self.log(.info, "request served in: {d}µs\n", .{finish - start});
        }
    }
};

fn worker(wg: *WaitGroup, router: Router) !void {
    var self = @as(*Router, @constCast(&router));

    try self.log(.debug, "Thread started ...\n", .{});
    var send_buffer: [4096]u8 = undefined;
    var read_buffer: [4096]u8 = undefined;

    while (true) {
        self.process(&send_buffer, &read_buffer) catch |err| {
            std.debug.print("ERROR ON self.process -- {any}\n", .{err});
            continue;
        };
        // break;
    }
    wg.finish();
    try self.log(.debug, "Thread finished ...\n", .{});
}

pub fn isStreamClosed(stream: std.net.Stream) bool {
    const result = stream.write(&[_]u8{0}) catch |err| {
        std.debug.print("stream write error {}\n", .{err});
        return true;
    };

    return result == 0;
}

pub fn isFileDescriptorOpen(fd: std.posix.socket_t) bool {
    const flags = std.os.linux.fcntl(fd, std.os.linux.F.GETFD, 0);
    std.debug.print("flags: {}\n", .{flags});
    return false;
}
