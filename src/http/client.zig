const std = @import("std");
const config = @import("../config/config.zig");

const http = std.http;
const assert = std.debug.assert;

const Allocator = std.mem.Allocator;
const Buffer = std.ArrayList(u8);

pub const content_type_json = "application/json";
pub const content_type_form = "application/x-www-form-urlencoded";
pub const content_type_text = "application/text/plain";

const HttpError = error{
    MethodNoSupportedByZigCurl,
};

pub const StdResponse = struct {
    allocator: Allocator,
    status_code: i32,
    body: ?Buffer,

    const Self = @This();
    pub fn init(allocator: Allocator) Self {
        return StdResponse{
            .allocator = allocator,
            .status_code = 0,
            .body = Buffer.init(allocator),
        };
    }

    pub fn deinit(self: *const Self) void {
        if (self.body) |body| {
            body.deinit();
        }
    }
};

pub fn fetch(
    allocator: Allocator,
    method: http.Method,
    url: []const u8,
    payload: ?[]const u8,
    content_type: []const u8,
    writer: std.fs.File.Writer,
    pretty: bool,
) !void {
    const response = try fetch_std(
        allocator,
        method,
        url,
        payload,
        content_type,
    );
    defer response.deinit();

    if (response.body) |body| {
        try print_json(
            allocator,
            &body.items,
            writer,
            pretty,
        );
    } else {
        const rbody = try std.fmt.allocPrint(
            allocator,
            "{d}",
            .{response.status_code},
        );
        defer allocator.free(rbody);

        try writer.writeAll(rbody);
    }
}

pub fn get_stream(
    allocator: Allocator,
    url: []const u8,
    content_type: []const u8,
    writer: std.fs.File.Writer,
) !void {
    var client = http.Client{ .allocator = allocator };
    defer client.deinit();
    const uri = try std.Uri.parse(url);
    var server_header_buffer: [16 * 1024]u8 = undefined;

    const headers = http.Client.Request.Headers{
        .content_type = .{
            .override = content_type,
        },
        .accept_encoding = .{
            .override = "deflate",
        },
    };

    const extra_headers = &[_]http.Header{
        .{
            .name = "Accept",
            .value = "*/*",
        },
    };

    var req = try client.open(.GET, uri, .{
        .server_header_buffer = &server_header_buffer,
        .headers = headers,
        .extra_headers = extra_headers,
    });
    defer req.deinit();

    try req.send();
    try req.finish();
    try req.wait();

    const reader = req.reader();
    while (true) {
        const byte = reader.readByte() catch |err| switch (err) {
            error.EndOfStream => return,
            else => |e| return e,
        };

        try writer.writeByte(byte);
    }
}

pub fn fetch_std(
    allocator: Allocator,
    method: http.Method,
    url: []const u8,
    payload: ?[]const u8,
    content_type: []const u8,
) !StdResponse {
    var client = http.Client{ .allocator = allocator };
    defer client.deinit();

    var response = StdResponse.init(allocator);
    const result = try http_fetch(&client, .{
        .payload = payload,
        .method = method,
        .location = .{
            .url = url,
        },
        .headers = http.Client.Request.Headers{
            .content_type = .{
                .override = content_type,
            },
            .accept_encoding = .{
                .override = "deflate",
            },
        },
        .extra_headers = &[_]http.Header{
            .{
                .name = "Accept",
                .value = "*/*",
            },
        },
        .response_storage = .{
            .dynamic = &response.body.?,
        },
    });
    response.status_code = @intFromEnum(result.status);

    return response;
}

fn json_prettify(allocator: Allocator, bytes: *const []u8, writer: std.fs.File.Writer) !void {
    const str = std.mem.bytesToValue([]u8, bytes);
    defer allocator.destroy(&str);

    const parsed = try std.json.parseFromSliceLeaky(
        std.json.Value,
        allocator,
        str,
        .{},
    );
    defer allocator.destroy(&parsed);

    try std.json.stringify(parsed, .{ .whitespace = .indent_4 }, writer);
}

pub fn print_json(allocator: Allocator, bytes: *const []u8, writer: std.fs.File.Writer, pretty: bool) !void {
    if (pretty) {
        try json_prettify(allocator, bytes, writer);
    } else {
        try writer.print("{s}", .{bytes.*});
    }
}

const FetchResult = struct {
    status: http.Status,
};

const Uri = std.Uri;

/// Perform a one-shot HTTP request with the provided options.
///
/// This function is threadsafe.
fn http_fetch(
    client: *std.http.Client,
    options: std.http.Client.FetchOptions,
) !FetchResult {
    const uri = switch (options.location) {
        .url => |u| try Uri.parse(u),
        .uri => |u| u,
    };
    var server_header_buffer: [16 * 1024]u8 = undefined;

    const method: http.Method = options.method orelse
        if (options.payload != null) .POST else .GET;

    var req = try client.open(method, uri, .{
        .server_header_buffer = options.server_header_buffer orelse &server_header_buffer,
        .redirect_behavior = options.redirect_behavior orelse
            if (options.payload == null) @enumFromInt(3) else .unhandled,
        .headers = options.headers,
        .extra_headers = options.extra_headers,
        .privileged_headers = options.privileged_headers,
        .keep_alive = options.keep_alive,
    });
    defer {
        req.deinit();
        if (req.connection) |conn| {
            client.allocator.destroy(conn);
        }
    }

    if (options.payload) |payload| req.transfer_encoding = .{ .content_length = payload.len };

    try req.send();

    if (options.payload) |payload| try req.writeAll(payload);

    try req.finish();
    try req.wait();

    switch (options.response_storage) {
        .ignore => {
            // Take advantage of request internals to discard the response body
            // and make the connection available for another request.
            req.response.skip = true;
            assert(try transferRead(&req, &.{}) == 0); // No buffer is necessary when skipping.
        },
        .dynamic => |list| {
            const max_append_size = options.max_append_size orelse 2 * 1024 * 1024;
            try req.reader().readAllArrayList(list, max_append_size);
        },
        .static => |list| {
            const buf = b: {
                const buf = list.unusedCapacitySlice();
                if (options.max_append_size) |len| {
                    if (len < buf.len) break :b buf[0..len];
                }
                break :b buf;
            };
            list.items.len += try req.reader().readAll(buf);
        },
    }

    return .{
        .status = req.response.status,
    };
}

fn transferRead(req: *std.http.Client.Request, buf: []u8) !usize {
    if (req.response.parser.done) return 0;

    var index: usize = 0;
    while (index == 0) {
        const amt = try req.response.parser.read(req.connection.?, buf[index..], req.response.skip);
        if (amt == 0 and req.response.parser.done) break;
        index += amt;
    }

    return index;
}
