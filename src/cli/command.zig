const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;

pub const MainArgs = struct {
    h: ?bool = false,

    pub const messages = .{
        .h = "print this help message",
    };
};

pub const Command = @This();
ptr: *const anyopaque,
vtable: *const VTable,

pub const VTable = struct {
    run: *const fn (
        self: *const anyopaque,
        allocator: std.mem.Allocator,
        pargs: ?*const anyopaque,
        args: [][:0]u8,
        writer: std.fs.File.Writer,
    ) anyerror!void,
    get_name: *const fn (ptr: *const anyopaque) []const u8,
    get_description: *const fn (ptr: *const anyopaque) []const u8,
};

pub fn get_name(self: Command) []const u8 {
    return self.vtable.get_name(self.ptr);
}

pub fn get_description(self: Command) []const u8 {
    return self.vtable.get_description(self.ptr);
}

pub fn run(
    self: Command,
    allocator: std.mem.Allocator,
    pargs: ?*const anyopaque,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) !void {
    return self.vtable.run(
        self.ptr,
        allocator,
        pargs,
        args,
        writer,
    );
}

pub fn init(obj: anytype, pargs: anytype) Command {
    const Ptr = @TypeOf(obj);
    const PtrInfo = @typeInfo(Ptr);
    std.debug.assert(PtrInfo == .Pointer); // Must be a pointer
    std.debug.assert(PtrInfo.Pointer.size == .One); // Must be a single-item pointer
    std.debug.assert(@typeInfo(PtrInfo.Pointer.child) == .Struct); // Must point to a struct

    const ArgsPtr = @TypeOf(pargs);
    const ArgsPtrInfo = @typeInfo(ArgsPtr);
    std.debug.assert(ArgsPtrInfo == .Pointer); // Must be a pointer
    std.debug.assert(ArgsPtrInfo.Pointer.size == .One); // Must be a single-item pointer
    std.debug.assert(@typeInfo(ArgsPtrInfo.Pointer.child) == .Struct); // Must point to a struct

    const impl = struct {
        fn get_name(ptr: *const anyopaque) []const u8 {
            const self = @as(Ptr, @ptrCast(@alignCast(ptr)));
            return self.name;
        }

        fn get_description(ptr: *const anyopaque) []const u8 {
            const self = @as(Ptr, @ptrCast(@alignCast(ptr)));
            return self.description;
        }

        fn run(
            ptr: *const anyopaque,
            allocator: std.mem.Allocator,
            pargs_ptr: ?*const anyopaque,
            args: [][:0]u8,
            writer: std.fs.File.Writer,
        ) !void {
            const self = @as(Ptr, @ptrCast(@alignCast(ptr)));
            if (!std.mem.eql(u8, self.name, args[0])) {
                return;
            }
            if (pargs_ptr) |pargs_ptr_c| {
                const parent_args = @as(ArgsPtr, @ptrCast(@alignCast(pargs_ptr_c)));
                return self.run(allocator, parent_args, args, writer);
            } else {
                return self.run(allocator, null, args, writer);
            }
        }
    };

    return .{
        .ptr = obj,
        .vtable = &.{
            .get_name = impl.get_name,
            .get_description = impl.get_description,
            .run = impl.run,
        },
    };
}

test "test creation of different obj with Command interface" {
    const CmdImplOne = struct {
        name: []const u8 = "cmd_impl_1",
        description: []const u8 = "cmd_impl_1 description",

        const Self = @This();
        pub fn get_name(self: *const Self) []const u8 {
            return self.name;
        }
        pub fn get_description(self: *const Self) []const u8 {
            return self.description;
        }
        pub fn run(
            self: *const Self,
            alloc: std.mem.Allocator,
            _: ?*const anyopaque,
            args: [][:0]u8,
            writer: std.fs.File.Writer,
        ) !void {
            if (std.mem.eql(u8, self.name, args[1])) {
                return;
            }

            const message = try std.fmt.allocPrint(
                alloc,
                "this will run on {s}\n",
                .{args[3]},
            );
            try writer.writeAll(message);
        }
    };
    const cmd_one_s = CmdImplOne{};
    const cmd_one = Command.init(&cmd_one_s);

    const stdout = std.io.getStdOut();
    var bw = std.io.bufferedWriter(stdout.writer());
    const w = bw.writer();

    const allocator = std.testing.allocator;
    var args = [_][:0]u8{
        try allocator.dupeZ(u8, "awesome-cli"),
        try allocator.dupeZ(u8, "cmd_impl_1"),
        try allocator.dupeZ(u8, "-u"),
        try allocator.dupeZ(u8, "the_url"),
    };
    defer for (args) |arg| {
        allocator.free(arg);
    };

    cmd_one.run(allocator, &MainArgs{ .url = "testing" }, &args, w) catch {
        std.debug.assert(false);
    };

    std.debug.assert(std.mem.eql(u8, cmd_one.get_name(), "cmd_impl_1"));
    std.debug.assert(std.mem.eql(u8, cmd_one.get_description(), "cmd_impl_1 description"));
}
