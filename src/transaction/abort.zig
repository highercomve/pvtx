const std = @import("std");
const fs = @import("../utils/fs.zig");

const Allocator = std.mem.Allocator;

pub fn exec(allocator: Allocator, path: []const u8) !void {
    const path_abs = try fs.parseUserPath(allocator, path);
    defer allocator.free(path_abs);

    var dir = try std.fs.cwd().openDir(path_abs, .{ .iterate = true });
    errdefer dir.close();

    var walker = try dir.walk(allocator);
    defer {
        walker.deinit();
        dir.close();
    }

    while (try walker.next()) |entry| {
        try dir.deleteTree(entry.path);
    }
}
