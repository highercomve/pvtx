const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");

const Allocator = mem.Allocator;
const JSON = @import("../pantavisor/state_mg.zig").JSON;

const pvclient = @import("../pantavisor/client.zig");
const state_mg = @import("../pantavisor/state_mg.zig");
const transaction = @import("./transaction.zig");

pub fn exec(
    allocator: Allocator,
    tpath: []const u8,
    rev: ?[]const u8,
    _: []const u8,
    writer: std.fs.File.Writer,
) !void {
    if (builtin.target.isMinGW()) {
        return;
    }

    var r: []u8 = undefined;
    defer allocator.free(r);

    if (rev == null) {
        r = try std.fmt.allocPrint(
            allocator,
            "locals/pvsm-{d}",
            .{std.time.milliTimestamp()},
        );
    } else {
        r = try std.fmt.allocPrint(
            allocator,
            "{s}",
            .{rev.?},
        );
    }

    const state = try transaction.getCurrentTState(allocator, tpath);
    defer state.deinit();

    var client = pvclient.init(allocator);
    defer client.deinit();

    const response = try client.commit(r, state.raw_json);
    defer response.deinit();

    if (response.body) |body| {
        try writer.print(
            "state had been commited {?s}\n",
            .{body.items},
        );
    }
}
