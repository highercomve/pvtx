const std = @import("std");
const mem = @import("std").mem;
const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");

pub fn exec(
    allocator: Allocator,
    trails_path: []const u8,
    rev: []const u8,
    writer: anytype,
) !void {
    const state_path = try std.fs.path.join(allocator, &[_][]const u8{
        trails_path,
        rev,
        ".pvr/json",
    });
    defer allocator.free(state_path);

    std.debug.print("getting current step state from: {s}\n", .{state_path});

    if (!fs.fileExists(state_path)) {
        return std.fs.File.OpenError.FileNotFound;
    }

    const file = try std.fs.cwd().openFile(
        state_path,
        .{ .mode = .read_only },
    );
    defer file.close();

    const content = try file.readToEndAlloc(allocator, 1024 * 1024);
    defer allocator.free(content);

    try writer.writeAll(content);
}
