const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");

const Allocator = mem.Allocator;

const Error = error{
    ObjectCannotBeLinked,
};

const state_mg = @import("../pantavisor/state_mg.zig");
const transaction = @import("./transaction.zig");
const fs = @import("../utils/fs.zig");

const PVConfig = struct {
    ObjectsDir: []const u8,
};

const posible_links = [_]struct { []const u8, []const u8 }{
    .{ "fit", ".pv/pantavisor.fit" },
    .{ "kernel", ".pv/pv-kernel.img" },
    .{ "linux", ".pv/pv-kernel.img" },
    .{ "pantavisor", ".pv/pv-initrd.img" },
    .{ "initrd", ".pv/pv-initrd.img" },
    .{ "fdt", ".pv/pv-fdt.dtb" },
};

pub fn exec(
    allocator: Allocator,
    tpath: []const u8,
    dpath: []const u8,
    opath: []const u8,
    writer: std.fs.File.Writer,
) !void {
    const tfile_path = try transaction.errorIfTDontExists(allocator, tpath);
    defer allocator.free(tfile_path);

    const transaction_file = try fs.openOrCreate(allocator, tfile_path);
    defer transaction_file.close();

    const state = try transaction.getCurrentTState(allocator, tpath);
    defer state.deinit();

    const deploy_path = try fs.parseUserPath(allocator, dpath);
    defer allocator.free(deploy_path);

    std.fs.cwd().makePath(deploy_path) catch |err| switch (err) {
        std.fs.Dir.MakeError.PathAlreadyExists => {
            try std.fs.cwd().deleteTree(deploy_path);
        },
        else => return err,
    };

    var destiny_folder = try fs.openOrCreateDir(allocator, deploy_path);
    defer destiny_folder.close();

    const objects_path = try fs.parseUserPath(allocator, opath);
    defer allocator.free(objects_path);

    for (state.json.value.object.keys(), state.json.value.object.values()) |key, value| {
        if (std.mem.eql(u8, key, "#spec")) {
            continue;
        }

        if (std.fs.path.dirname(key)) |dir| {
            destiny_folder.makePath(dir) catch |err| switch (err) {
                std.fs.Dir.MakeError.PathAlreadyExists => {
                    std.debug.print("ERROR: makePath {s} -- {}\n", .{ dir, err });
                },
                else => return err,
            };
        }

        const file = destiny_folder.createFile(
            key,
            .{ .truncate = true },
        ) catch |err| {
            std.debug.print("ERROR: creating file {s} -- {}\n", .{ key, err });
            return err;
        };
        defer file.close();

        switch (value) {
            .string => {
                if (isValidSHA256(value.string)) {
                    try linkObject(
                        allocator,
                        deploy_path,
                        objects_path,
                        key,
                        value.string,
                    );
                } else {
                    const content = try std.json.stringifyAlloc(
                        allocator,
                        value,
                        .{ .whitespace = .minified },
                    );
                    defer allocator.free(content);

                    try file.writeAll(content);
                }
            },
            else => {
                const content = try std.json.stringifyAlloc(
                    allocator,
                    value,
                    .{ .whitespace = .minified },
                );
                defer allocator.free(content);

                try file.writeAll(content);
            },
        }

        try writer.print("file created {s}\n", .{key});
    }

    const pv_dot_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ deploy_path, ".pv" },
    );
    defer allocator.free(pv_dot_path);

    destiny_folder.makePath(pv_dot_path) catch |err| switch (err) {
        std.fs.Dir.MakeError.PathAlreadyExists => {},
        else => {
            std.debug.print("error making {s} -- {}", .{ pv_dot_path, err });
            return err;
        },
    };

    const pvr_dot_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ deploy_path, ".pvr" },
    );
    defer allocator.free(pvr_dot_path);

    destiny_folder.makePath(pvr_dot_path) catch |err| switch (err) {
        std.fs.Dir.MakeError.PathAlreadyExists => {},
        else => {
            std.debug.print("error making {s} -- {}", .{ pvr_dot_path, err });
            return err;
        },
    };

    const pvr_json_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ deploy_path, ".pvr/json" },
    );
    defer allocator.free(pvr_json_path);

    if (fs.fileExists(pvr_json_path)) {
        const pvr_json_file = fs.truncateFile(allocator, pvr_json_path) catch |err| {
            std.debug.print("error truncateFile {s} -- {}", .{ pvr_json_path, err });
            return err;
        };
        defer pvr_json_file.close();

        const w = pvr_json_file.writer();
        const content = try std.json.stringifyAlloc(allocator, state.json.value, .{ .whitespace = .minified });
        defer allocator.free(content);

        try w.writeAll(content);
    } else {
        const pvr_json_file = fs.openOrCreate(allocator, pvr_json_path) catch |err| {
            std.debug.print("error openOrCreate {s} -- {}", .{ pvr_json_path, err });
            return err;
        };
        defer pvr_json_file.close();

        const w = pvr_json_file.writer();
        const content = try std.json.stringifyAlloc(allocator, state.json.value, .{ .whitespace = .minified });
        defer allocator.free(content);

        try w.writeAll(content);
    }

    const pvr_config_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ deploy_path, ".pvr/config" },
    );
    defer allocator.free(pvr_config_path);

    if (!fs.fileExists(pvr_config_path)) {
        const pvr_config_file = fs.openOrCreate(allocator, pvr_config_path) catch |err| {
            std.debug.print("error openOrCreate {s} -- {}", .{ pvr_config_path, err });
            return err;
        };
        defer pvr_config_file.close();

        const relative_objects_path = try std.fs.path.relative(allocator, deploy_path, objects_path);
        defer allocator.free(relative_objects_path);

        const config = PVConfig{ .ObjectsDir = relative_objects_path };
        const content = try std.json.stringifyAlloc(allocator, config, .{ .whitespace = .minified });
        defer allocator.free(content);

        try pvr_config_file.writeAll(content);
    }

    if (state.json.value.object.get("bsp/run.json")) |runjson| {
        for (posible_links) |posible_link| {
            if (runjson.object.get(posible_link[0])) |object| {
                const object_key = try std.fmt.allocPrint(allocator, "bsp/{s}", .{object.string});
                defer allocator.free(object_key);

                if (state.json.value.object.get(object_key)) |o| {
                    try linkObject(
                        allocator,
                        deploy_path,
                        objects_path,
                        posible_link[1],
                        o.string,
                    );
                }
            }
        }
    }
}

fn linkObject(
    allocator: Allocator,
    deploy_path: []const u8,
    objects_path: []const u8,
    key: []const u8,
    value: []const u8,
) !void {
    var deploy_dir = fs.openOrCreateDir(allocator, deploy_path) catch |err| {
        std.debug.print("ERROR openOrCreateDir {s} -- {}", .{ deploy_path, err });
        return err;
    };
    defer deploy_dir.close();

    var objects_dir = std.fs.cwd().openDir(objects_path, .{}) catch |err| {
        std.debug.print("ERROR openDir {s} -- {}", .{ objects_path, err });
        return err;
    };
    defer objects_dir.close();

    const pv_object_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ deploy_path, key },
    );
    defer allocator.free(pv_object_path);

    const pv_obj_path_abs = try fs.parseUserPath(allocator, pv_object_path);
    defer allocator.free(pv_obj_path_abs);

    const file_existed = fs.fileExists(pv_obj_path_abs);

    if (file_existed) {
        try std.fs.cwd().deleteFile(pv_obj_path_abs);
    }

    const object_path = try std.fs.path.join(
        allocator,
        &[_][]const u8{ objects_path, value },
    );
    defer allocator.free(object_path);

    if (!fs.fileExists(object_path)) {
        return Error.ObjectCannotBeLinked;
    }

    if (builtin.os.tag == .linux) {
        try std.posix.linkat(
            deploy_dir.fd,
            object_path,
            objects_dir.fd,
            pv_object_path,
            0,
        );
    } else {
        try deploy_dir.symLink(object_path, pv_object_path, .{});
    }
}

fn isHexChar(c: u8) bool {
    return (c >= '0' and c <= '9') or (c >= 'a' and c <= 'f') or (c >= 'A' and c <= 'F');
}

fn isValidSHA256(str: []const u8) bool {
    if (str.len != 64) return false;

    for (str) |c| {
        if (!isHexChar(c)) {
            return false;
        }
    }

    return true;
}
