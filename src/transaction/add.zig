const std = @import("std");
const mem = @import("std").mem;

const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");
const state_mg = @import("../pantavisor/state_mg.zig");
const models = @import("../pantavisor/models.zig");
const unpack = @import("./unpack.zig");
const transaction = @import("./transaction.zig");

const Pool = std.Thread.Pool;
const max_tar_file: usize = 1024 * 1000 * 32;

pub fn exec(
    allocator: Allocator,
    o_path: []const u8,
    tpath: []const u8,
    p_path: []const u8,
    writer: std.fs.File.Writer,
) !void {
    return unpack.exec(
        allocator,
        o_path,
        tpath,
        p_path,
        writer,
    );
}
