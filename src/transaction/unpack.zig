const std = @import("std");
const mem = @import("std").mem;

const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");
const state_mg = @import("../pantavisor/state_mg.zig");
const models = @import("../pantavisor/models.zig");
const transaction = @import("./transaction.zig");

const Pool = std.Thread.Pool;
const max_tar_file: usize = 1024 * 1000 * 32;

pub fn exec(
    allocator: Allocator,
    o_path: []const u8,
    t_path: []const u8,
    p_path: []const u8,
    writer: std.fs.File.Writer,
) !void {
    const package_content = fs.readFileOrStdin(
        allocator,
        p_path,
        max_tar_file,
    ) catch |err| {
        std.debug.print(
            "error in read_file_or_stdin: {any}\n",
            .{err},
        );
        return err;
    };
    defer allocator.free(package_content);

    const json = try fromContent(
        allocator,
        package_content,
        o_path,
        t_path,
        writer,
    );
    defer allocator.free(json);

    try writer.print(
        "extracted the object in {s} and merge the state\n",
        .{o_path},
    );
}

pub fn fromContent(
    allocator: Allocator,
    package_content: []const u8,
    o_path: []const u8,
    t_path: []const u8,
    writer: std.fs.File.Writer,
) ![]const u8 {
    const tfile_path = try transaction.errorIfTDontExists(allocator, t_path);
    defer allocator.free(tfile_path);

    const is_gzip = fs.isGzipContent(package_content);
    const is_tar = fs.isTarData(package_content);

    const state = try transaction.getCurrentTState(allocator, t_path);
    defer state.deinit();

    if (is_gzip or is_tar) {
        var stream = std.io.fixedBufferStream(package_content);

        if (is_gzip) {
            var decompress = std.compress.gzip.decompressor(stream.reader());
            try processFile(
                allocator,
                decompress.reader(),
                o_path,
                state,
                writer,
            );
        } else {
            try processFile(
                allocator,
                stream.reader(),
                o_path,
                state,
                writer,
            );
        }
    } else {
        try state_mg.mergeStateFromBuffer(
            allocator,
            state,
            package_content,
        );
    }

    const new_json = try std.json.stringifyAlloc(
        allocator,
        state.json.value,
        .{ .whitespace = .minified },
    );

    const transaction_file = try fs.truncateFile(
        allocator,
        tfile_path,
    );
    defer transaction_file.close();

    try transaction_file.writeAll(new_json);

    const tpath_abs = try fs.parseUserPath(allocator, t_path);
    defer allocator.free(tpath_abs);

    const signatures_file_p = try std.fs.path.join(
        allocator,
        &[_][]const u8{ tpath_abs, models.signatures_file_name },
    );
    defer allocator.free(signatures_file_p);

    const signatures_file = try fs.truncateFile(allocator, signatures_file_p);
    defer signatures_file.close();

    const signatures = try state_mg.processSignatures(
        allocator,
        state.*.json,
    );
    defer signatures.deinit();

    const signatures_json = try std.json.stringifyAlloc(
        allocator,
        signatures.parts,
        .{ .whitespace = .minified },
    );
    defer allocator.free(signatures_json);

    try signatures_file.writeAll(signatures_json);

    return new_json;
}

fn processFile(
    allocator: Allocator,
    reader: anytype,
    o_path: []const u8,
    state: *state_mg.State,
    writer: std.fs.File.Writer,
) !void {
    var file_name_buffer: [std.fs.MAX_PATH_BYTES]u8 = undefined;
    var link_name_buffer: [std.fs.MAX_PATH_BYTES]u8 = undefined;

    var files = std.tar.iterator(reader, .{
        .file_name_buffer = &file_name_buffer,
        .link_name_buffer = &link_name_buffer,
    });

    var objects_dir = try fs.openOrCreateDir(allocator, o_path);
    defer objects_dir.close();

    var pool = Pool{
        .allocator = allocator,
        .threads = &[_]std.Thread{},
    };
    try pool.init(.{ .allocator = allocator });
    defer pool.deinit();

    var wg = std.Thread.WaitGroup{};
    wg.reset();

    var progress_bar = fs.ProgressBar.init(allocator);
    defer progress_bar.deinit();

    while (try files.next()) |file| {
        switch (file.kind) {
            .file => {
                if (std.mem.eql(u8, file.name, "json")) {
                    try writer.print("patching state {s}...\n", .{file.name});
                    try state_mg.mergeState(
                        allocator,
                        state,
                        file.reader(),
                        @as(usize, @intCast(file.size)),
                    );
                    continue;
                }

                if (std.mem.startsWith(u8, file.name, "objects/")) {
                    const file_name = try std.mem.replaceOwned(
                        u8,
                        allocator,
                        file.name,
                        "objects/",
                        "",
                    );
                    const object_file = try objects_dir.createFile(
                        file_name,
                        .{ .truncate = true },
                    );
                    try progress_bar.addStatus(
                        file_name,
                        @as(usize, @intCast(file.size)),
                    );
                    pool.spawnWg(
                        &wg,
                        copyFiles,
                        .{ file_name, &file, &object_file, &progress_bar },
                    );
                    pool.waitAndWork(&wg);
                }
            },
            else => {
                return models.TError.FileIsNotPvExport;
            },
        }
    }

    try progress_bar.printFinish();
}

fn copyFiles(
    file_name: []const u8,
    file: anytype,
    out: *const std.fs.File,
    pg: *fs.ProgressBar,
) void {
    defer out.close();
    fs.readAndWrite(
        file_name,
        file,
        out.writer(),
        pg,
    ) catch return;
}
