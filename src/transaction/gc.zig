const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");

const Allocator = mem.Allocator;

const pvclient = @import("../pantavisor/client.zig");

pub fn exec(
    allocator: Allocator,
    writer: std.fs.File.Writer,
) !void {
    if (builtin.target.isMinGW()) {
        return;
    }
    var client = pvclient.init(allocator);
    defer client.deinit();

    const response = try client.gc();
    defer response.deinit();

    try writer.print("running garbage collector\n", .{});
}
