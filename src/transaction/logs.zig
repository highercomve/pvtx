const std = @import("std");
const mem = @import("std").mem;
const server = @import("../http/server.zig");

const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");

const File = struct {
    file: std.fs.File,
    reader: std.fs.File.Reader,
    path: []const u8,
    size: usize = 0,
    read: usize = 0,
};

pub const Writer = union(enum) {
    any: std.io.AnyWriter,
    http: *server.HttpResponse,
    fs: std.fs.File.Writer,
};

pub const Error = error{
    CanNotOpenLogsFolder,
    CanNotIterateOverLogsFolder,
    CanNotIterateNextLogsFolder,
    CanNotAddToSources,
    WriterClosed,
};

pub fn exec(
    allocator: Allocator,
    logs_path: []const u8,
    follow: bool,
    rev: []const u8,
    _: usize,
    sources: std.ArrayList([]const u8),
    writer: Writer,
) !void {
    var files = std.ArrayList(*File).init(allocator);
    defer files.deinit();

    var res: ?*std.http.Server.Response = null;
    switch (writer) {
        .http => |w| {
            res = w.response;
        },
        else => {},
    }

    var paths = std.ArrayList([]const u8).init(allocator);
    defer {
        for (paths.items) |p| {
            allocator.free(p);
        }
        paths.deinit();
    }

    if (sources.items.len == 0) {
        const rev_logs_path = try std.fs.path.join(
            allocator,
            &[_][]const u8{ logs_path, rev },
        );
        defer allocator.free(rev_logs_path);

        var logsDir = std.fs.cwd().openDir(
            rev_logs_path,
            .{ .iterate = true },
        ) catch {
            return Error.CanNotOpenLogsFolder;
        };
        defer logsDir.close();

        var iterator = logsDir.walk(allocator) catch {
            return Error.CanNotIterateOverLogsFolder;
        };
        defer iterator.deinit();

        while (iterator.next() catch {
            return Error.CanNotIterateNextLogsFolder;
        }) |entry| {
            switch (entry.kind) {
                .file => {
                    const source = try allocator.dupe(u8, entry.path);
                    paths.append(source) catch {
                        return Error.CanNotAddToSources;
                    };
                },
                else => {
                    continue;
                },
            }
        }
    } else {
        for (sources.items) |s| {
            const copyS = try allocator.dupe(u8, s);
            try paths.append(copyS);

            const path = try std.fs.path.join(
                allocator,
                &[_][]const u8{ logs_path, rev, s },
            );
            defer allocator.free(path);

            const stat = std.fs.cwd().statFile(path) catch |err| {
                std.debug.print("can't stat path {s} {}\n", .{ path, err });
                return Error.CanNotOpenLogsFolder;
            };
            if (stat.kind != .directory) {
                continue;
            }

            var subDir = std.fs.cwd().openDir(
                path,
                .{ .iterate = true },
            ) catch |err| {
                std.debug.print("can't open directory path {s} {}\n", .{ path, err });
                return Error.CanNotOpenLogsFolder;
            };
            defer subDir.close();

            var iterator = subDir.walk(allocator) catch {
                return Error.CanNotIterateOverLogsFolder;
            };
            while (iterator.next() catch {
                return Error.CanNotIterateNextLogsFolder;
            }) |entry| {
                switch (entry.kind) {
                    .file => {
                        const source = try allocator.dupe(u8, entry.path);
                        paths.append(source) catch {
                            return Error.CanNotAddToSources;
                        };
                    },
                    else => {
                        continue;
                    },
                }
            }
        }
    }

    for (paths.items) |arg| {
        const path = try std.fs.path.join(allocator, &[_][]const u8{
            logs_path,
            rev,
            arg,
        });
        defer allocator.free(path);

        const stat = std.fs.cwd().statFile(path) catch |err| switch (err) {
            error.FileNotFound => {
                std.debug.print("path not found {s}\n", .{path});
                continue;
            },
            else => return err,
        };
        if (stat.kind != .file) {
            continue;
        }

        const file = try std.fs.cwd().openFile(
            path,
            .{ .mode = .read_only },
        );

        const reader = file.reader();

        var f = try allocator.create(File);

        f.path = arg;
        f.file = file;
        f.reader = reader;
        f.read = 0;
        f.size = @as(usize, @intCast(stat.size));
        try files.append(f);
    }

    var buf: [4096]u8 = undefined;
    const ff = try files.toOwnedSlice();
    defer {
        for (ff) |f| {
            allocator.destroy(f);
        }
        allocator.free(ff);
    }

    var i: usize = 0;
    while (true) {
        for (ff) |f| {
            if (f.read > 0 and res != null) {
                try res.?.flush();
            }
            while (true) {
                const read_result = f.reader.readUntilDelimiterOrEof(
                    buf[0..],
                    '\n',
                ) catch |err| {
                    std.debug.print("read file {s} {}", .{ f.path, err });
                    return err;
                };
                if (read_result) |bytes| {
                    f.read += bytes.len;
                    const line = try std.fmt.allocPrint(
                        allocator,
                        "[{s}] {s}\n",
                        .{ f.path, bytes },
                    );
                    defer allocator.free(line);
                    switch (writer) {
                        .http => |w| {
                            const writen = w.response.write(line) catch 0;
                            if (writen == 0) {
                                return Error.WriterClosed;
                            }
                        },
                        .any => |w| {
                            try w.writeAll(line);
                        },
                        .fs => |w| {
                            try w.writeAll(line);
                        },
                    }
                } else {
                    i = i + 1;
                    if (i >= 2) {
                        switch (writer) {
                            .http => |w| {
                                if (w.stream_closed()) {
                                    return error.WriterClosed;
                                }
                            },
                            else => {},
                        }
                        i = 0;
                    }
                    break;
                }
            }
        }
        if (!follow) {
            return;
        }
        std.time.sleep(1 * std.time.ns_per_s);
    }
}

fn min(a: usize, b: usize) usize {
    if (a < b) {
        return a;
    }

    return b;
}
