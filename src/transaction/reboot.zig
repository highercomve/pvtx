const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");

const Allocator = mem.Allocator;

const pvclient = @import("../pantavisor/client.zig");

pub fn exec(
    allocator: Allocator,
    message: ?[]const u8,
    writer: anytype,
) !void {
    if (builtin.target.isMinGW()) {
        return;
    }
    var client = pvclient.init(allocator);
    defer client.deinit();

    const response = try client.reboot(message);
    defer response.deinit();

    try writer.print("rebooting device {s}\n", .{message orelse ""});
}
