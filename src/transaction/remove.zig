const std = @import("std");
const mem = @import("std").mem;
const Allocator = mem.Allocator;

const state_mg = @import("../pantavisor/state_mg.zig");
const transaction = @import("./transaction.zig");
const fs = @import("../utils/fs.zig");

pub fn exec(
    allocator: Allocator,
    tpath: []const u8,
    parts: [][]const u8,
    writer: std.fs.File.Writer,
) ![]const u8 {
    const tfile_path = try transaction.errorIfTDontExists(allocator, tpath);
    defer allocator.free(tfile_path);

    const transaction_file = try fs.openOrCreate(allocator, tfile_path);
    defer transaction_file.close();

    const stat = try transaction_file.stat();
    var state = try transaction.getCurrentTState(allocator, tpath);
    defer state.deinit();

    for (parts) |part| {
        const part_signature = try std.fmt.allocPrint(
            allocator,
            "{s}{s}{s}",
            .{ "_sigs/", part, ".json" },
        );
        defer allocator.free(part_signature);

        try writer.print("removing {s}\n", .{part});
        try writer.print("removing {s}\n", .{part_signature});

        state_mg.removeFromState(state, part) catch |err| {
            try writer.print("error on delete part {}", .{err});
        };
        state_mg.removeFromState(state, part_signature) catch |err| {
            try writer.print("error on delete part {}", .{err});
        };
        try writer.print(" success!\n", .{});
    }

    const new_json = try std.json.stringifyAlloc(
        allocator,
        state.json.value,
        .{ .whitespace = .minified },
    );

    if (stat.size > new_json.len) {
        const new_file = try fs.truncateFile(allocator, tfile_path);
        defer new_file.close();

        try new_file.writeAll(new_json);
    } else {
        try transaction_file.writeAll(new_json);
    }

    return new_json;
}
