const std = @import("std");
const mem = @import("std").mem;

const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");
const state_mg = @import("../pantavisor/state_mg.zig");
const models = @import("../pantavisor/models.zig");
const transaction = @import("./transaction.zig");

pub fn exec(
    allocator: Allocator,
    spath: []const u8,
    tpath: []const u8,
) !*state_mg.State {
    const tfile_path = try transaction.errorIfTExists(allocator, tpath);
    defer allocator.free(tfile_path);

    var dir = try fs.openOrCreateDir(allocator, tpath);
    defer dir.close();

    const tpath_abs = try fs.parseUserPath(allocator, tpath);
    defer allocator.free(tpath_abs);

    const transaction_file = try fs.openOrCreate(allocator, tfile_path);
    defer transaction_file.close();

    const signatures_file_p = try std.fs.path.join(
        allocator,
        &[_][]const u8{ tpath_abs, models.signatures_file_name },
    );
    defer allocator.free(signatures_file_p);

    const signatures_file = try fs.openOrCreate(allocator, signatures_file_p);
    defer signatures_file.close();

    const state = try state_mg.getStateFromFile(allocator, spath);

    try transaction_file.writeAll(state.raw_json);

    const signatures_json = try std.json.stringifyAlloc(
        allocator,
        state.signatures.parts,
        .{ .whitespace = .minified },
    );
    defer allocator.free(signatures_json);

    _ = try signatures_file.write(signatures_json);

    return state;
}
