const std = @import("std");
const mem = @import("std").mem;
const Allocator = mem.Allocator;

const state_mg = @import("../pantavisor/state_mg.zig");
const transaction = @import("./transaction.zig");

pub fn exec(
    allocator: Allocator,
    tpath: []const u8,
    pretty: bool,
    writer: anytype,
) !void {
    const state = try transaction.getCurrentTState(allocator, tpath);
    defer state.deinit();

    if (pretty) {
        const json = try std.json.stringifyAlloc(
            allocator,
            state.json.value,
            .{ .whitespace = .indent_tab },
        );
        defer allocator.free(json);

        try writer.print("{s}\n", .{json});
    } else {
        try writer.print("{s}\n", .{state.raw_json});
    }
}
