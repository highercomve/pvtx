const std = @import("std");
const mem = @import("std").mem;

const Allocator = mem.Allocator;
const models = @import("../pantavisor/models.zig");
const fs = @import("../utils/fs.zig");
const stage_mg = @import("../pantavisor/state_mg.zig");

pub fn errorIfTDontExists(allocator: Allocator, tpath: []const u8) ![]const u8 {
    const tpath_abs = try fs.parseUserPath(allocator, tpath);
    defer allocator.free(tpath_abs);

    const transaction_file_p = try std.fs.path.join(
        allocator,
        &[_][]const u8{ tpath_abs, models.state_file_name },
    );

    if (!fs.fileExists(transaction_file_p)) {
        return models.TError.TDoesntExists;
    }

    return transaction_file_p;
}

pub fn errorIfTExists(allocator: Allocator, tpath: []const u8) ![]const u8 {
    const tpath_abs = try fs.parseUserPath(allocator, tpath);
    defer allocator.free(tpath_abs);

    const transaction_file_p = try std.fs.path.join(
        allocator,
        &[_][]const u8{ tpath_abs, models.state_file_name },
    );

    if (fs.fileExists(transaction_file_p)) {
        defer allocator.free(transaction_file_p);
        return models.TError.TAlreadyExists;
    }

    return transaction_file_p;
}

pub fn getCurrentTState(allocator: Allocator, tpath: []const u8) !*stage_mg.State {
    const file_path = try errorIfTDontExists(allocator, tpath);
    defer allocator.free(file_path);

    return stage_mg.getStateFromFile(allocator, file_path);
}
