const std = @import("std");
const mem = @import("std").mem;
const Allocator = mem.Allocator;

const fs = @import("../utils/fs.zig");
const current_rev = "/pantavisor/device-meta.pantavisor/revision";

pub fn exec(
    allocator: Allocator,
    trails_path: []const u8,
    rev: []const u8,
    writer: anytype,
) !void {
    const progress_path = try std.fs.path.join(allocator, &[_][]const u8{
        trails_path,
        rev,
        ".pv/progress",
    });
    defer allocator.free(progress_path);

    std.debug.print("searching for progress in {s}\n", .{progress_path});

    if (!fs.fileExists(progress_path)) {
        return std.fs.File.OpenError.FileNotFound;
    }

    const progress = try fs.readFileAndTrimNewlines(allocator, progress_path);
    defer allocator.free(progress);

    if (fs.fileExists(current_rev)) {
        const running_rev_file = try std.fs.openFileAbsolute(
            current_rev,
            .{ .mode = .read_only },
        );
        defer running_rev_file.close();

        const running_rev = try running_rev_file.readToEndAlloc(
            allocator,
            1024,
        );
        defer allocator.free(running_rev);

        const msg = try std.fmt.allocPrint(
            allocator,
            "{{ \"progress\": {s}, \"rev\": \"{s}\" }}",
            .{ progress, running_rev },
        );
        defer allocator.free(msg);
        try writer.writeAll(msg);
    } else {
        const msg = try std.fmt.allocPrint(
            allocator,
            "{{ \"progress\": {s}, \"rev\": \"{s}\" }}",
            .{ progress, rev },
        );
        defer allocator.free(msg);

        try writer.writeAll(msg);
    }
}
