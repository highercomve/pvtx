const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");
const Allocator = mem.Allocator;

const pvclient = @import("../pantavisor/client.zig");

pub fn exec(
    allocator: Allocator,
    writer: anytype,
) !void {
    if (builtin.target.isMinGW()) {
        return;
    }
    var client = pvclient.init(allocator);
    defer client.deinit();

    const dm_response = try client.getDevmeta();
    defer dm_response.deinit();

    const um_response = try client.getUsermeta();
    defer um_response.deinit();

    const config_response = try client.getConfig();
    defer config_response.deinit();

    try writer.print(
        "{{ \"device\": {s}, \"user\": {s}, \"config\": {s} }}",
        .{
            dm_response.body.?.items,
            um_response.body.?.items,
            config_response.body.?.items,
        },
    );
}
