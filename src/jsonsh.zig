const std = @import("std");
const mem = @import("std").mem;
const Allocator = @import("std").mem.Allocator;
const Writer = std.fs.File.Writer;

const args_parser = @import("cli/argsparser.zig");
const config = @import("config/config.zig");
const fs = @import("utils/fs.zig");

const JSON = std.json.Parsed(std.json.Value);
const maxjsonsize: usize = 1024 * 1000;

pub const Args = struct {
    help: ?bool = false,

    pub const shorts = .{
        .help = .h,
    };

    pub const messages = .{
        .help = "print this help message",
    };
};

const defaultDepth: i32 = -1;
const Buffer = std.ArrayList([]u8);

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();
    defer {
        if (config.is_debug(allocator)) {
            std.log.debug("Memory usage: {d}", .{arena.state.end_index});
        }
    }

    const stdout = std.io.getStdOut().writer();
    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    const opt = args_parser.parse(
        allocator,
        Args,
        args,
        "[depth] [FilePath]",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    defer {
        if (config.is_debug(allocator)) {
            printMemoryUsage(allocator, "end of program") catch {
                stdout.print("error printing memory", .{}) catch {};
            };
        }
    }

    if (opt.positional_args.items.len > 2) {
        try opt.print_help(stdout);
        return;
    }
    var depth: i32 = -1;
    var path = try std.fmt.allocPrint(allocator, "-", .{});
    defer allocator.free(path);

    if (opt.positional_args.items.len == 2) {
        depth = try std.fmt.parseInt(i32, opt.positional_args.items[0], 10);
        path = try std.fmt.allocPrint(
            allocator,
            "{s}",
            .{opt.positional_args.items[1]},
        );
    }

    if (opt.positional_args.items.len == 1) {
        depth = std.fmt.parseInt(
            i32,
            opt.positional_args.items[0],
            10,
        ) catch defaultDepth;
        if (depth == -1) {
            path = try std.fmt.allocPrint(
                allocator,
                "{s}",
                .{opt.positional_args.items[0]},
            );
        }
    }

    try getJsonShell(
        allocator,
        path,
        depth,
        stdout,
    );
}

fn getJsonShell(
    allocator: Allocator,
    path: []const u8,
    depth: i32,
    writer: Writer,
) !void {
    const data = try fs.readFileOrStdin(
        allocator,
        path,
        maxjsonsize,
    );
    defer allocator.free(data);

    const json = try std.json.parseFromSlice(
        std.json.Value,
        allocator,
        data,
        .{ .allocate = .alloc_always },
    );
    defer json.deinit();

    try getJson(
        allocator,
        "",
        writer,
        json.value,
        0,
        depth,
    );
}

fn getJson(
    allocator: Allocator,
    key: []u8,
    writer: Writer,
    json: std.json.Value,
    count: u32,
    depth: i32,
) !void {
    var separator = try std.fmt.allocPrint(allocator, "", .{});
    if (!std.mem.eql(u8, key, "")) {
        separator = try std.fmt.allocPrint(allocator, ",", .{});
    }
    defer allocator.free(separator);

    switch (json) {
        .array => {
            if (count == depth) {
                const value_string = try std.json.stringifyAlloc(
                    allocator,
                    json,
                    .{},
                );
                defer allocator.free(value_string);

                const value = try std.fmt.allocPrint(
                    allocator,
                    "[{s}]\t{s}",
                    .{ key, value_string },
                );
                defer allocator.free(value);

                return try writer.print("{s}\n", .{value});
            } else {
                for (json.array.items, 0..) |item, i| {
                    const child_key = try std.fmt.allocPrint(
                        allocator,
                        "{s}{s}\"{d}\"",
                        .{ key, separator, i },
                    );
                    defer allocator.free(child_key);

                    try getJson(
                        allocator,
                        child_key,
                        writer,
                        item,
                        count + 1,
                        depth,
                    );
                }
            }
        },
        .object => {
            if (count == depth) {
                const value_string = try std.json.stringifyAlloc(
                    allocator,
                    json,
                    .{},
                );
                defer allocator.free(value_string);

                const value = try std.fmt.allocPrint(
                    allocator,
                    "[{s}]\t{s}",
                    .{ key, value_string },
                );
                defer allocator.free(value);

                return try writer.print("{s}\n", .{value});
            } else {
                for (json.object.keys()) |i| {
                    const child_key = try std.fmt.allocPrint(
                        allocator,
                        "{s}{s}\"{s}\"",
                        .{ key, separator, i },
                    );
                    defer allocator.free(child_key);

                    const value = json.object.get(i);
                    try getJson(
                        allocator,
                        child_key,
                        writer,
                        value.?,
                        count + 1,
                        depth,
                    );
                }
            }
        },
        .float => {
            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\t{d}",
                .{ key, json.float },
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
        .integer => {
            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\t{d}",
                .{ key, json.integer },
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
        .string => {
            const string_value = try std.mem.replaceOwned(
                u8,
                allocator,
                json.string,
                "\n",
                "\\n",
            );
            defer allocator.free(string_value);

            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\t\"{s}\"",
                .{ key, string_value },
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
        .number_string => {
            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\t{s}",
                .{ key, json.number_string },
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
        .bool => {
            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\t{}",
                .{ key, json.bool },
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
        .null => {
            const value = try std.fmt.allocPrint(
                allocator,
                "[{s}]\tnull",
                .{key},
            );
            defer allocator.free(value);

            return try writer.print("{s}\n", .{value});
        },
    }
}

fn printMemoryUsage(allocator: std.mem.Allocator, stage: []const u8) !void {
    const writer = std.io.getStdErr().writer();
    const proc_status_path = "/proc/self/status";

    var file = try std.fs.cwd().openFile(proc_status_path, .{ .mode = .read_only });
    defer file.close();

    const read_bytes = try file.readToEndAlloc(allocator, 4096);
    defer allocator.free(read_bytes);

    try writer.print("{s}\n{s}", .{ stage, read_bytes });
}
