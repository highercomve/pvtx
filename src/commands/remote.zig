const std = @import("std");
const Allocator = @import("std").mem.Allocator;
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const config = @import("../config/config.zig");

const abort = @import("../remote/abort.zig");
const begin = @import("../remote/begin.zig");
const remove = @import("../remote/remove.zig");
const add = @import("../remote/add.zig");
const commit = @import("../remote/commit.zig");
const show = @import("../remote/show.zig");
const status = @import("../remote/status.zig");
const steps = @import("../remote/steps.zig");
const run_rev = @import("../remote/run.zig");
const reboot = @import("../remote/reboot.zig");
const get_config = @import("../remote/get_config.zig");
const logs = @import("../remote/logs.zig");
const deploy = @import("../remote/deploy.zig");
const gc = @import("../remote/gc.zig");

pub const RemoteArguments = struct {
    h: ?bool = false,
    url: ?[]const u8 = null,

    pub const shorts = .{
        .url = .u,
    };

    pub const messages = .{
        .h = "print this help message",
        .url = "device pvsm base url",
    };
};

const subcomands = [_]cmd.Command{
    abort.init(),
    begin.init(),
    remove.init(),
    add.init(),
    commit.init(),
    deploy.init(),
    show.init(),
    status.init(),
    steps.init(),
    run_rev.init(),
    reboot.init(),
    get_config.init(),
    logs.init(),
    gc.init(),
};

// Command definiton
const Command = @This();
name: []const u8 = "remote",
description: []const u8 = "manage remote pvsm application",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &cmd.MainArgs{});
}

pub fn run(
    _: *const Command,
    a: Allocator,
    _: ?*const cmd.MainArgs,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var arena = std.heap.ArenaAllocator.init(a);
    defer arena.deinit();
    const allocator = arena.allocator();

    var opt = args_parser.parse(
        allocator,
        RemoteArguments,
        args,
        "remote device manager",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.args.url == null) {
        try print_help(allocator, writer);
        return;
    }

    if (opt.args.h.?) {
        try print_help(allocator, writer);
        return;
    }

    if (opt.positional_args.items.len > 0) {
        const diff = args.len - opt.positional_args.items.len;
        const subcommand = opt.positional_args.items[0];

        inline for (subcomands) |c| {
            if (mem.eql(u8, subcommand, c.get_name())) {
                try c.run(allocator, &opt.args, args[diff..], writer);
                return;
            }
        }
    }

    try print_help(allocator, writer);
}

pub fn print_help(allocator: std.mem.Allocator, writer: anytype) !void {
    var subcommands_buf: [subcomands.len][]const u8 = undefined;
    defer {
        for (subcommands_buf) |b| {
            allocator.free(b);
        }
    }

    var i: u32 = 0;
    inline for (subcomands) |c| {
        subcommands_buf[i] = try std.fmt.allocPrint(
            allocator,
            "[{s}]: {s}",
            .{ c.get_name(), c.get_description() },
        );
        i = i + 1;
    }
    const header_tmpl =
        \\ USAGE:
        \\     pvsm remote [SUBCOMMANDS]
        \\
        \\ OPTIONS:
        \\    - help            cli help
        \\    - url,u           device url, example: http://192.168.217.2:12369 (required)
        \\
        \\ SUBCOMMANDS:
        \\
    ;

    const m1 = try std.fmt.allocPrint(allocator, header_tmpl, .{});
    defer allocator.free(m1);

    try writer.writeAll(m1);

    for (subcommands_buf[0..subcomands.len]) |c| {
        const cmd_line = try std.fmt.allocPrint(
            allocator,
            "   - {s}\n",
            .{c},
        );
        defer allocator.free(cmd_line);
        defer allocator.free(c);

        try writer.writeAll(cmd_line);
    }

    const footer_tmpl =
        \\
        \\ Use help of any subcommands to get more information
        \\
    ;

    const m2 = try std.fmt.allocPrint(allocator, footer_tmpl, .{});
    defer allocator.free(m2);

    try writer.writeAll(m2);
}
