const std = @import("std");
const Allocator = @import("std").mem.Allocator;
const http = @import("std").http;
const mem = @import("std").mem;
const builtin = @import("builtin");

const http_server = @import("../http/server.zig");
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const config = @import("../config/config.zig");
const models = @import("../pantavisor/models.zig");
const fs = @import("../utils/fs.zig");
const create_new_state = @import("../pantavisor/state_mg.zig").create_new_state;
const parseBool = @import("../utils/strings.zig").parseBool;
const pvclient = @import("../pantavisor/client.zig");

const begin = @import("../transaction/begin.zig");
const unpack = @import("../transaction/unpack.zig");
const abort = @import("../transaction/abort.zig");
const remove = @import("../transaction/remove.zig");
const commit = @import("../transaction/commit.zig");
const deploy = @import("../transaction/deploy.zig");
const revrun = @import("../transaction/run.zig");
const show = @import("../transaction/show.zig");
const status = @import("../transaction/status.zig");
const steps = @import("../transaction/steps.zig");
const get_config = @import("../transaction/get_config.zig");
const reboot = @import("../transaction/reboot.zig");
const gc = @import("../transaction/gc.zig");
const logs = @import("../transaction/logs.zig");

const HttpRequest = http_server.HttpRequest;
const HttpResponse = http_server.HttpResponse;

var transaction_path: []const u8 = undefined;
var objects_path: []const u8 = undefined;
var trails_path: []const u8 = undefined;
var files_path: []const u8 = undefined;
var logs_path: []const u8 = undefined;
const default_n: usize = 10;

pub const ServerArguments = struct {
    help: ?bool = false,
    ip: ?[]const u8 = "0.0.0.0",
    port: ?u16 = 12369,
    theards: ?usize = 1,

    transaction: ?[]const u8 = "~/.pvsm",
    objects: ?[]const u8 = "/storage/objects",
    trails: ?[]const u8 = "/storage/trails",
    files: ?[]const u8 = "/app",
    logs: ?[]const u8 = "/pantavisor/logs",

    pub const shorts = .{
        .port = .p,
        .theards = .t,
        .help = .h,
        .transaction = .s,
        .objects = .o,
        .trails = .r,
        .logs = .l,
    };

    pub const messages = .{
        .ip = "server exposed ip address",
        .port = "server export port",
        .theards = "numbers of theard to use",
        .transaction = "transaction folder path",
        .objects = "objects folder path",
        .trails = "trails folder path",
        .logs = "logs folder path",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "serve",
description: []const u8 = "start pvsm server application",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &cmd.MainArgs{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    _: ?*const cmd.MainArgs,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        ServerArguments,
        args,
        "pvsm server",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.args.help.?) {
        try opt.print_help(writer);
        return;
    }

    try writer.print("Trying to starting Router server...\n", .{});

    var router = try http_server.Router.init(
        allocator,
        opt.args.ip.?,
        opt.args.port.?,
        .debug,
        opt.args.theards.?,
    );
    defer router.deinit();

    transaction_path = opt.args.transaction.?;
    objects_path = opt.args.objects.?;
    trails_path = opt.args.trails.?;
    files_path = opt.args.files.?;
    logs_path = opt.args.logs.?;

    // pvtx api
    try router.add_route(.POST, "/cgi-bin/pvtx/begin", handle_begin);
    try router.add_route(.POST, "/cgi-bin/pvtx/abort", handle_abort);
    try router.add_route(.PUT, "/cgi-bin/pvtx/add", handle_add);
    try router.add_route(.POST, "/cgi-bin/pvtx/remove", handle_remove);
    try router.add_route(.PUT, "/cgi-bin/pvtx/remove", handle_remove);
    try router.add_route(.GET, "/cgi-bin/pvtx/show", handle_show);
    try router.add_route(.POST, "/cgi-bin/pvtx/deploy", handle_deploy);
    try router.add_route(.POST, "/cgi-bin/pvtx/commit", handle_commit);
    try router.add_route(.POST, "/cgi-bin/pvtx/run", handle_run);
    try router.add_route(.GET, "/cgi-bin/pvtx/steps", handle_steps);
    try router.add_route(.GET, "/cgi-bin/pvtx/status", handle_status);
    try router.add_route(.GET, "/cgi-bin/pvtx/get-config", handle_get_config);
    try router.add_route(.POST, "/cgi-bin/pvtx/reboot", handle_reboot);
    try router.add_route(.POST, "/cgi-bin/pvtx/gc", handle_gc);
    try router.add_route(.GET, "/cgi-bin/pvtx/logs", handle_logs);
    try router.add_route(.GET, "/cgi-bin/pvtx/logs-parts", handle_logs_parts);

    // pvr api
    try router.add_route(.GET, "/cgi-bin/logs", handle_logs);
    try router.add_route(.GET, "/cgi-bin/steps", handle_pv_steps);
    try router.add_route(.GET, "/cgi-bin/state", not_implemented);
    try router.add_route(.POST, "/cgi-bin/state", not_implemented);
    try router.add_route(.POST, "/cgi-bin/run", not_implemented);
    try router.add_route(.POST, "/cgi-bin/reboot", not_implemented);
    try router.add_route(.GET, "/cgi-bin/raw", not_implemented);
    try router.add_route(.PUT, "/cgi-bin/raw", not_implemented);
    try router.add_route(.GET, "/cgi-bin/pvr", not_implemented);
    try router.add_route(.GET, "/cgi-bin/objects", not_implemented);
    try router.add_route(.GET, "/cgi-bin/objects", not_implemented);
    try router.add_route(.POST, "/cgi-bin/golocal", not_implemented);
    try router.add_route(.GET, "/cgi-bin/device-meta", not_implemented);
    try router.add_route(.POST, "/cgi-bin/device-meta", not_implemented);
    try router.add_route(.GET, "/cgi-bin/user-meta", not_implemented);
    try router.add_route(.POST, "/cgi-bin/user-meta", not_implemented);
    try router.add_route(.POST, "/cgi-bin/deploy", not_implemented);
    try router.add_route(.GET, "/cgi-bin/.pvrremote", not_implemented);

    // handle all static files
    try router.add_route(.GET, "*", handle_files);

    try router.listen();
}

fn not_implemented(_: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    res.status = .ok;
    try res.setContent("method not implemented yet");
}

fn handle_files(alloc: Allocator, req: *HttpRequest, res: *HttpResponse) !void {
    const file_relative_path = try std.mem.replaceOwned(
        u8,
        alloc,
        req.uri.path.percent_encoded,
        "/app",
        "",
    );
    defer alloc.free(file_relative_path);

    var file_abs_path = try std.fs.path.join(alloc, &[_][]const u8{
        files_path,
        file_relative_path,
    });
    defer alloc.free(file_abs_path);

    // const encoding = req.get_header("accept-encoding");

    var file: ?std.fs.File = null;
    if (fs.fileExists(file_abs_path)) {
        file = try std.fs.cwd().openFile(
            file_abs_path,
            .{ .mode = .read_only },
        );
    } else {
        file_abs_path = try std.fs.path.join(
            alloc,
            &[_][]const u8{ files_path, "index.html" },
        );
        if (fs.fileExists(file_abs_path)) {
            file = try std.fs.cwd().openFile(
                file_abs_path,
                .{ .mode = .read_only },
            );
        }
    }

    if (file == null) {
        res.status = .not_found;
        try res.setContent("{\"status\": \"file not found\"}");
        return;
    }

    const content_type = fs.mimeTypeFromFilename(file_abs_path);
    std.debug.print("content_type: {s}\n", .{content_type});
    std.debug.print("file_abs_path: {s}\n", .{file_abs_path});

    const f = file.?;
    defer f.close();

    res.status = .ok;
    try res.setContent(try f.readToEndAlloc(alloc, 1024 * 1024));
    try res.setHeaders(.{
        .extra_headers = &[_]http.Header{
            http.Header{
                .name = "Cache-Control",
                .value = "private, max-age=86400, stale-while-revalidate=604800",
            },
            http.Header{
                .name = "Content-Type",
                .value = content_type,
            },
        },
    });
}

fn handle_steps(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    try res.setHeaders(.{});

    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var rev: []const u8 = "current";
    const rev_query = queries.get("rev");
    if (rev_query) |q| {
        if (q.items.len > 0) {
            if (!std.mem.eql(u8, q.items[0], "")) {
                rev = q.items[0];
            }
        }
    }

    const writer = res.response.writer();
    steps.exec(
        alloc,
        trails_path,
        rev,
        writer,
    ) catch |err| {
        const body = try std.fmt.allocPrint(
            alloc,
            "{{\"error\": \"{any}\"}}",
            .{err},
        );
        res.status = .internal_server_error;
        try res.setContent(body);
        return;
    };
    res.status = .ok;
    try res.setContent(null);
}

fn handle_status(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    try res.setHeaders(.{});

    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var rev: []const u8 = "current";
    const rev_query = queries.get("rev");
    if (rev_query) |q| {
        if (q.items.len > 0) {
            if (!std.mem.eql(u8, q.items[0], "")) {
                rev = q.items[0];
            }
        }
    }

    const writer = res.response.writer();
    status.exec(
        alloc,
        trails_path,
        rev,
        writer,
    ) catch |err| {
        const body = try std.fmt.allocPrint(
            alloc,
            "{{\"error\": \"{any}\"}}",
            .{err},
        );
        res.status = .internal_server_error;
        try res.setContent(body);
        return;
    };
    res.status = .ok;
    try res.setContent(null);
}

fn handle_show(alloc: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    try res.setHeaders(.{});
    const writer = res.response.writer();
    show.exec(
        alloc,
        transaction_path,
        true,
        writer,
    ) catch |err| switch (err) {
        models.TError.TDoesntExists => {
            res.status = .not_found;
            try res.setContent("{\"error\": \"transaction not found\"}");
            return;
        },
        else => {
            const body = try std.fmt.allocPrint(alloc, "{{\"error\": \"{any}\"}}", .{err});
            res.status = .internal_server_error;
            try res.setContent(body);
            return;
        },
    };
}

fn handle_begin(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var path = try std.fs.path.join(
        alloc,
        &[_][]const u8{ trails_path, "current" },
    );
    defer alloc.free(path);

    if (queries.get("empty")) |value| {
        if (value.items.len > 0 and std.mem.eql(u8, value.items[0], "true")) {
            path = try std.fmt.allocPrint(alloc, "{s}", .{create_new_state});
        }
    }

    const state = begin.exec(
        alloc,
        path,
        transaction_path,
    ) catch |err| switch (err) {
        models.TError.TAlreadyExists => {
            res.status = .forbidden;
            try res.setContent("{\"error\": \"there is a transaction already running, you can abort or deploy before begin a new one.\"}");
            return;
        },
        else => {
            const body = try std.fmt.allocPrint(alloc, "{{\"error\": \"{any}\"}}", .{err});
            res.status = .internal_server_error;
            try res.setContent(body);
            return;
        },
    };
    defer state.deinit();

    const content = try alloc.alloc(u8, state.raw_json.len);
    defer alloc.free(content);
    std.mem.copyForwards(u8, content, state.raw_json);
    try res.setContent(content);
}

fn handle_abort(alloc: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    abort.exec(
        alloc,
        transaction_path,
    ) catch |err| {
        const body = try std.fmt.allocPrint(alloc, "{{\"error\": \"{any}\"}}", .{err});
        res.status = .internal_server_error;
        try res.setContent(body);
        return;
    };

    try res.setContent("{\"status\": \"ok\"}");
}

fn handle_add(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    std.debug.print(
        "Reading file with size {?d}\n",
        .{request.request.head.content_length},
    );
    if (request.request.head.content_length) |cl| {
        const buffer = try alloc.alloc(u8, @as(usize, @intCast(cl)));
        defer alloc.free(buffer);

        const reader = try request.request.reader();
        const read = try reader.readAll(buffer);
        if (read != cl) {
            res.status = .internal_server_error;
            const message = try std.fmt.allocPrint(
                alloc,
                "{{ \"err\": \"could not read the whole file, read {d} of {d}\"}}",
                .{ read, cl },
            );
            try res.setContent(message);
            return;
        }

        const stderrFile = std.io.getStdErr();
        const stderr = stderrFile.writer();
        const json = unpack.fromContent(
            alloc,
            buffer,
            objects_path,
            transaction_path,
            stderr,
        ) catch |err| {
            res.status = .internal_server_error;
            const message = try std.fmt.allocPrint(
                alloc,
                "{{ \"err\": \"error unpacking package -- {}\"}}",
                .{err},
            );
            try res.setContent(message);
            return;
        };

        res.status = .ok;
        try res.setContent(json);
        return;
    }

    std.debug.print("aqui nunca llega\n", .{});

    res.status = .bad_request;
    try res.setContent("request without data");
}

fn handle_remove(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    if (request.uri.query == null) {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"url needs the ?part query to know what parts will be removed\"}");
    }

    var queries = try request.get_query();
    defer deinit_queries(&queries);

    if (queries.get("part")) |parts| {
        defer parts.deinit();

        const stderr_file = std.io.getStdErr();
        const stderr = stderr_file.writer();
        const json = try remove.exec(
            alloc,
            transaction_path,
            parts.items,
            stderr,
        );

        res.status = .ok;
        try res.setContent(json);
        return;
    }

    res.status = .no_content;
    try res.setContent("{\"error\": \"url needs the ?part query to know what parts will be removed\"}");
}

fn handle_commit(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var revision: []u8 = undefined;
    defer alloc.free(revision);

    if (queries.get("part")) |parts| {
        defer parts.deinit();

        if (parts.items.len > 0) {
            revision = @as([]u8, @constCast(parts.items[0]));
        } else {
            revision = try std.fmt.allocPrint(
                alloc,
                "locals/pvsm-{d}",
                .{std.time.milliTimestamp()},
            );
        }
    } else {
        revision = try std.fmt.allocPrint(
            alloc,
            "locals/pvsm-{d}",
            .{std.time.milliTimestamp()},
        );
    }

    const stderr_file = std.io.getStdErr();
    const stderr = stderr_file.writer();

    const dpath = try std.fmt.allocPrint(alloc, "{s}/{s}", .{ trails_path, revision });
    defer alloc.free(dpath);

    deploy.exec(
        alloc,
        transaction_path,
        dpath,
        objects_path,
        stderr,
    ) catch |err| {
        std.debug.print("error on deploy -- {any}\n", .{err});
        return err;
    };

    abort.exec(
        alloc,
        transaction_path,
    ) catch |err| {
        std.debug.print("error on abort -- {any}\n", .{err});
        return err;
    };

    const body = try std.fmt.allocPrint(
        alloc,
        "{{\"revision\": \"{s}\"}}",
        .{revision},
    );

    res.status = .ok;
    try res.setContent(body);
}

fn handle_deploy(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var d_path: []u8 = undefined;
    var d: []u8 = undefined;
    defer alloc.free(d);
    defer alloc.free(d_path);

    if (queries.get("part")) |parts| {
        defer parts.deinit();

        if (parts.items.len > 0) {
            d = @as([]u8, @constCast(parts.items[0]));
            d_path = try std.fs.path.join(alloc, &[_][]const u8{
                trails_path,
                d,
            });
        } else {
            d = try std.fmt.allocPrint(
                alloc,
                "locals/pvsm-{d}",
                .{std.time.milliTimestamp()},
            );
            d_path = try std.fs.path.join(alloc, &[_][]const u8{
                trails_path,
                d,
            });
        }
    } else {
        d = try std.fmt.allocPrint(
            alloc,
            "locals/pvsm-{d}",
            .{std.time.milliTimestamp()},
        );
        d_path = try std.fs.path.join(alloc, &[_][]const u8{
            trails_path,
            d,
        });
    }

    const stderr_file = std.io.getStdErr();
    const stderr = stderr_file.writer();
    deploy.exec(
        alloc,
        transaction_path,
        d_path,
        objects_path,
        stderr,
    ) catch |err| {
        std.debug.print("{any}\n", .{err});
        const body = try std.fmt.allocPrint(
            alloc,
            "{{\"error\": \"{}\"}}",
            .{err},
        );

        res.status = .internal_server_error;
        try res.setContent(body);
        return;
    };

    abort.exec(
        alloc,
        transaction_path,
    ) catch |err| {
        const body = try std.fmt.allocPrint(
            alloc,
            "{{\"error\": \"{}\"}}",
            .{err},
        );

        res.status = .internal_server_error;
        try res.setContent(body);
        return;
    };

    const body = try std.fmt.allocPrint(
        alloc,
        "{{\"revision\": \"{s}\"}}",
        .{d},
    );

    res.status = .ok;
    try res.setContent(body);
}

fn handle_run(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var queries = try request.get_query();
    defer deinit_queries(&queries);

    if (queries.get("rev")) |revs| {
        if (revs.items.len == 0) {
            res.status = .bad_request;
            try res.setContent("query params rev is required");
            return;
        }

        const rev = revs.items[0];
        const stderr_file = std.io.getStdErr();
        const stderr = stderr_file.writer();
        try revrun.exec(
            alloc,
            rev,
            stderr,
        );

        res.status = .ok;
        try res.setContent("{\"status\": \"done\"}");
    } else {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"the rev parameter is required\"}");
    }
}

fn handle_reboot(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var queries = try request.get_query();
    defer deinit_queries(&queries);

    var message: []u8 = undefined;
    defer alloc.free(message);

    if (queries.get("message")) |m| {
        if (m.items.len > 0) {
            message = try std.fmt.allocPrint(
                alloc,
                "{s}",
                .{m.items[0]},
            );
        }
    } else {
        message = try std.fmt.allocPrint(
            alloc,
            "reboot from api",
            .{},
        );
    }

    const writer = res.response.writer();

    res.status = .ok;
    try res.setHeaders(.{});
    reboot.exec(
        alloc,
        message,
        writer,
    ) catch {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"error with socket communication\"}");
    };
}

fn handle_get_config(alloc: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    const writer = res.response.writer();

    res.status = .ok;
    try res.setHeaders(.{});
    get_config.exec(
        alloc,
        writer,
    ) catch {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"error with socket communication\"}");
    };
}

fn handle_logs(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var query = try request.get_query();
    defer deinit_queries(&query);

    var follow = false;
    var rev: []const u8 = "current";
    var n: usize = default_n;
    var sources = std.ArrayList([]const u8).init(alloc);
    defer sources.deinit();

    if (query.get("follow")) |f| {
        if (f.items.len > 0) {
            follow = parseBool(f.items[0]);
        }
    }

    if (query.get("rev")) |s| {
        if (s.items.len > 0) {
            rev = s.items[0];
        }
    }

    if (query.get("source")) |s| {
        if (s.items.len > 0) {
            for (s.items) |ss| {
                if (!std.mem.eql(u8, ss, "/")) {
                    try sources.append(ss);
                }
            }
        }
    }

    if (query.get("n")) |s| {
        if (s.items.len > 0) {
            n = std.fmt.parseInt(usize, s.items[0], 10) catch default_n;
        }
    }

    res.status = .ok;
    try res.setContent(null);
    try res.setHeaders(
        .{
            .transfer_encoding = .none,
            .extra_headers = &[_]http.Header{
                http.Header{
                    .name = "Content-Type",
                    .value = "text/plain;charset=UTF-8",
                },
            },
        },
    );

    logs.exec(
        alloc,
        logs_path,
        follow,
        rev,
        n,
        sources,
        logs.Writer{ .http = res },
    ) catch |err| switch (err) {
        logs.Error.WriterClosed => {
            try res.response.flush();
            return;
        },
        else => {
            const message = try std.fmt.allocPrint(
                alloc,
                "{{\"error\": \"with logs reading -- {}\"}}",
                .{err},
            );
            res.status = .bad_request;

            std.debug.print("{s}\n", .{message});
            try res.setHeaders(
                .{
                    .transfer_encoding = .{ .content_length = message.len },
                    .extra_headers = &[_]http.Header{
                        http.Header{ .name = "Content-Type", .value = "application/json" },
                    },
                },
            );
            try res.setContent(message);
            return;
        },
    };
}

fn handle_gc(alloc: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    const stderr = std.io.getStdErr();
    const writer = stderr.writer();

    gc.exec(
        alloc,
        writer,
    ) catch {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"error with socket communication\"}");
        return;
    };

    res.status = .ok;
    try res.setContent("{\"status\": \"ok\"}");
}

fn handle_pv_steps(alloc: Allocator, _: *HttpRequest, res: *HttpResponse) !void {
    if (builtin.target.isMinGW()) {
        return;
    }

    var client = pvclient.init(alloc);
    defer client.deinit();

    var steps_res = client.getSteps(null) catch |err| {
        const message = try std.fmt.allocPrint(
            alloc,
            "{{\"error\": \"error with socket communication -- {}\"}}",
            .{err},
        );
        res.status = .bad_request;
        try res.setContent(message);
        return;
    };
    defer steps_res.deinit();

    if (steps_res.body == null) {
        res.status = .bad_request;
        try res.setContent("{\"error\": \"pvcontrol empty answer on list of steps\"}");
        return;
    }

    res.status = .ok;
    try res.setContent(try steps_res.body.?.toOwnedSlice());
}

fn handle_logs_parts(alloc: Allocator, request: *HttpRequest, res: *HttpResponse) !void {
    var query = try request.get_query();
    defer deinit_queries(&query);

    var sources = std.json.Array.init(alloc);
    defer {
        for (sources.items) |s| {
            alloc.free(s.string);
        }
        sources.deinit();
    }
    try sources.append(std.json.Value{ .string = "" });

    var rev: []const u8 = "current";
    if (query.get("rev")) |s| {
        if (s.items.len > 0) {
            rev = s.items[0];
        }
    }

    const rev_logs_path = try std.fs.path.join(
        alloc,
        &[_][]const u8{ logs_path, rev },
    );
    defer alloc.free(rev_logs_path);

    var logsDir = try std.fs.cwd().openDir(
        rev_logs_path,
        .{ .iterate = true },
    );
    defer logsDir.close();

    var iterator = try logsDir.walk(alloc);
    defer iterator.deinit();

    while (try iterator.next()) |entry| {
        switch (entry.kind) {
            .file => {
                const source = try alloc.dupe(u8, entry.path);
                try sources.append(std.json.Value{ .string = source });
            },
            else => {
                continue;
            },
        }
    }

    var items = std.json.ObjectMap.init(alloc);
    defer items.deinit();

    try items.put("items", std.json.Value{ .array = sources });
    const json = std.json.Value{ .object = items };

    const content = try std.json.stringifyAlloc(
        alloc,
        json,
        .{},
    );

    res.status = .ok;
    try res.setContent(content);
}

fn deinit_queries(queries: *http_server.QueryMap) void {
    defer queries.deinit();

    var iterator = queries.keyIterator();
    while (iterator.next()) |key| {
        const posible_val = queries.get(key.*);
        if (posible_val) |val| {
            val.deinit();
        }
    }
}
