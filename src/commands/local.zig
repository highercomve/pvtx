const std = @import("std");
const Allocator = @import("std").mem.Allocator;
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const config = @import("../config/config.zig");

const abort = @import("../local/abort.zig");
const begin = @import("../local/begin.zig");
const remove = @import("../local/remove.zig");
const add = @import("../local/add.zig");
const commit = @import("../local/commit.zig");
const show = @import("../local/show.zig");
const status = @import("../local/status.zig");
const steps = @import("../local/steps.zig");
const run_rev = @import("../local/run.zig");
const reboot = @import("../local/reboot.zig");
const get_config = @import("../local/get_config.zig");
const deploy = @import("../local/deploy.zig");
const unpack = @import("../local/unpack.zig");
const gc = @import("../local/gc.zig");
const logs = @import("../local/logs.zig");

pub const LocalArguments = struct {
    h: ?bool = false,

    pub const shorts = .{};

    pub const messages = .{
        .h = "print this help message",
    };
};

const subcomands = [_]cmd.Command{
    abort.init(),
    begin.init(),
    remove.init(),
    add.init(),
    commit.init(),
    deploy.init(),
    show.init(),
    status.init(),
    steps.init(),
    run_rev.init(),
    reboot.init(),
    get_config.init(),
    unpack.init(),
    gc.init(),
    logs.init(),
};

// Command definiton
const Command = @This();
name: []const u8 = "local",
description: []const u8 = "manage local pvsm actions",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &cmd.MainArgs{});
}

pub fn run(
    _: *const Command,
    a: Allocator,
    _: ?*const cmd.MainArgs,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var arena = std.heap.ArenaAllocator.init(a);
    defer arena.deinit();
    const allocator = arena.allocator();

    var opt = args_parser.parse(
        allocator,
        LocalArguments,
        args,
        "local device manager",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.args.h.?) {
        try print_help(allocator, writer);
        return;
    }

    if (opt.positional_args.items.len > 0) {
        const diff = args.len - opt.positional_args.items.len;
        const subcommand = opt.positional_args.items[0];

        inline for (subcomands) |c| {
            if (mem.eql(u8, subcommand, c.get_name())) {
                try c.run(allocator, &opt.args, args[diff..], writer);
                return;
            }
        }
    }

    try print_help(allocator, writer);
}

pub fn print_help(allocator: std.mem.Allocator, writer: anytype) !void {
    var subcommands_buf: [subcomands.len][]const u8 = undefined;
    defer {
        for (subcommands_buf) |buf| {
            allocator.free(buf);
        }
    }

    var i: u32 = 0;
    inline for (subcomands) |c| {
        subcommands_buf[i] = try std.fmt.allocPrint(
            allocator,
            "[{s}]: {s}",
            .{ c.get_name(), c.get_description() },
        );
        i = i + 1;
    }
    const header_tmpl =
        \\ USAGE:
        \\     pvsm local [SUBCOMMANDS]
        \\
        \\ OPTIONS:
        \\    - h help
        \\
        \\ SUBCOMMANDS:
        \\
    ;

    const m1 = try std.fmt.allocPrint(allocator, header_tmpl, .{});
    defer allocator.free(m1);

    try writer.writeAll(m1);

    for (subcommands_buf[0..subcomands.len]) |c| {
        const cmd_line = try std.fmt.allocPrint(
            allocator,
            "   - {s}\n",
            .{c},
        );
        defer allocator.free(cmd_line);

        try writer.writeAll(cmd_line);
    }

    const footer_tmpl =
        \\
        \\ Use help of any subcommands to get more information
        \\
    ;

    const m2 = try std.fmt.allocPrint(allocator, footer_tmpl, .{});
    defer allocator.free(m2);

    try writer.writeAll(m2);
}
