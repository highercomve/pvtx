const std = @import("std");
const mem = @import("std").mem;
const builtin = @import("builtin");
const args_parser = @import("cli/argsparser.zig");
const config = @import("config/config.zig");
const command = @import("cli/command.zig");

const local = @import("commands/local.zig");
const remote = @import("commands/remote.zig");
const serve = @import("commands/serve.zig");

const Allocator = mem.Allocator;

const subcomands = [_]command.Command{
    local.init(),
    remote.init(),
    serve.init(),
};

pub fn main() !void {
    const gpa_config = config.getGpaConfig();
    var gpa = std.heap.GeneralPurposeAllocator(gpa_config){};
    defer {
        if (builtin.mode == .Debug) {
            const check = gpa.deinit();
            const std_file = std.io.getStdErr();
            defer std_file.close();
            const stderr = std_file.writer();
            stderr.print("\n{any}\n", .{check}) catch {};
        } else {
            _ = gpa.deinit();
        }
    }
    const allocator = gpa.allocator();

    const std_file = std.io.getStdOut();
    const stdout = std_file.writer();
    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    const opt = args_parser.parse(
        allocator,
        command.MainArgs,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    defer {
        if (config.is_debug(allocator)) {
            printMemoryUsage(allocator, "end of program") catch {
                stdout.print("error printing memory", .{}) catch {};
            };
        }
    }

    if (opt.args.h.?) {
        try print_help(allocator, stdout);
        return;
    }

    if (opt.positional_args.items.len > 0) {
        const diff = args.len - opt.positional_args.items.len;
        const subcommand = opt.positional_args.items[0];

        inline for (subcomands) |cmd| {
            if (mem.eql(u8, subcommand, cmd.get_name())) {
                try cmd.run(allocator, &opt.args, args[diff..], stdout);
                return;
            }
        }
    }

    try print_help(allocator, stdout);
}

fn printMemoryUsage(allocator: std.mem.Allocator, stage: []const u8) !void {
    const std_file = std.io.getStdErr();
    defer std_file.close();

    const writer = std_file.writer();
    const proc_status_path = "/proc/self/status";

    var file = try std.fs.cwd().openFile(proc_status_path, .{ .mode = .read_only });
    defer file.close();

    const read_bytes = try file.readToEndAlloc(allocator, 4096);

    try writer.print("{s}\n{s}", .{ stage, read_bytes });
}

pub fn print_help(allocator: std.mem.Allocator, writer: anytype) !void {
    var subcommands_buf: [subcomands.len][]const u8 = undefined;

    var i: u32 = 0;
    inline for (subcomands) |cmd| {
        subcommands_buf[i] = try std.fmt.allocPrint(
            allocator,
            "[{s}]: {s}",
            .{ cmd.get_name(), cmd.get_description() },
        );
        i = i + 1;
    }
    defer {
        for (subcommands_buf) |buf| {
            allocator.free(buf);
        }
    }

    const header_tmpl =
        \\ USAGE:
        \\     pvsm [SUBCOMMANDS]
        \\
        \\ OPTIONS:
        \\    - h cli help
        \\
        \\ SUBCOMMANDS:
        \\
    ;

    const m1 = try std.fmt.allocPrint(allocator, header_tmpl, .{});
    defer allocator.free(m1);

    try writer.writeAll(m1);

    for (subcommands_buf[0..subcomands.len]) |cmd| {
        const cmd_line = try std.fmt.allocPrint(
            allocator,
            "   - {s}\n",
            .{cmd},
        );
        defer allocator.free(cmd_line);

        try writer.writeAll(cmd_line);
    }

    const footer_tmpl =
        \\
        \\ Use help of any subcommands to get more information
        \\
    ;

    const m2 = try std.fmt.allocPrint(allocator, footer_tmpl, .{});
    defer allocator.free(m2);

    try writer.writeAll(m2);
}
