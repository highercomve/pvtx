const std = @import("std");
const mg = @import("./state_mg.zig");

// Global allocator
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const a = gpa.allocator();

// Export the function with C ABI
export fn MergeStates(
    src: [*c]const u8,
    src_len: c_int,
    patch: [*c]const u8,
    patch_len: c_int,
    out_len: *c_int,
) [*c]u8 {
    var arena = std.heap.ArenaAllocator.init(a);
    const allocator = arena.allocator();
    defer arena.deinit();

    // Ensure src and patch are non-null
    if (src == null or patch == null) {
        out_len.* = 0;
        return null;
    }

    // Convert C strings to Zig slices safely
    const src_slice = src[0..@as(usize, @intCast(src_len))];
    const patch_slice = patch[0..@as(usize, @intCast(patch_len))];

    // Call the mergeStateBuffer function
    const result = mg.mergeStateBuffer(allocator, src_slice, patch_slice) catch |err| {
        // Handle error
        std.debug.print("Error in mergeStateBuffer: {}\n", .{err});
        out_len.* = 0;
        return null;
    };

    // Check if the result length fits within c_int limits
    if (result.len > @as(usize, std.math.maxInt(c_int))) {
        std.debug.print("Result length exceeds c_int maximum\n", .{});
        out_len.* = 0;
        return null;
    }

    // Set the output length safely
    out_len.* = @as(c_int, @intCast(result.len));

    // Allocate memory for the C string result
    const c_result = a.alloc(u8, result.len) catch |err| {
        std.debug.print("Error allocating memory: {}\n", .{err});
        out_len.* = 0;
        return null;
    };

    // Copy the result to the allocated memory
    @memcpy(c_result, result);

    // Return the result as a C string
    return c_result.ptr;
}

// Export a function to remove state using mg.removeStateBuffer
export fn RemoveFromState(
    src: [*c]const u8,
    src_len: c_int,
    key: [*c]const u8,
    out_len: *c_int,
) [*c]u8 {
    var arena = std.heap.ArenaAllocator.init(a);
    const allocator = arena.allocator();
    defer arena.deinit();

    // Ensure src and key are non-null
    if (src == null or key == null) {
        out_len.* = 0;
        return null;
    }

    // Convert C strings to Zig slices safely
    const src_slice = src[0..@as(usize, @intCast(src_len))];
    const key_slice = key[0..std.mem.len(key)];

    // Call the removeState function
    const result = mg.removeStateBuffer(allocator, src_slice, key_slice) catch |err| {
        std.debug.print("Error in removeState: {}\n", .{err});
        out_len.* = 0;
        return null;
    };
    defer allocator.free(result);

    // Check if the result length fits within c_int limits
    if (result.len > @as(usize, std.math.maxInt(c_int))) {
        std.debug.print("Result length exceeds c_int maximum\n", .{});
        out_len.* = 0;
        return null;
    }

    // Set the output length safely
    out_len.* = @intCast(result.len);

    // Allocate memory for the C string result
    const c_result = a.alloc(u8, result.len) catch |err| {
        std.debug.print("Error allocating memory: {}\n", .{err});
        out_len.* = 0;
        return null;
    };

    // Copy the result to the allocated memory
    @memcpy(c_result, result);

    // Return the result as a C string
    return c_result.ptr;
}

export fn Deinit() void {
    _ = gpa.deinit();
}

// Export a function to free the memory allocated by MergeStates
export fn FreeMem(ptr: [*c]u8, len: c_int) void {
    if (ptr == null or len <= 0) return;

    // Convert the pointer to a non-null slice safely
    const slice = @as([*]u8, @ptrCast(ptr))[0..@as(usize, @intCast(len))];
    a.free(slice);
}
