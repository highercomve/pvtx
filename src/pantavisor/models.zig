pub const TError = error{
    TAlreadyExists,
    TDoesntExists,
    FileIsNotPvExport,
    CantDeleteElementFromState,
};

pub const state_file_name = "state.json";
pub const config_file_name = "config.json";
pub const signatures_file_name = "signatures.json";

pub const t_exists_m =
    \\ transaction already exists!
    \\ you need to close current transaction using:
    \\ - abort
    \\ - deploy
    \\ - commit
    \\
;
pub const t_dont_exists_m =
    \\ transaction does not exists!
    \\ start a new transaction using:
    \\ - begin
    \\
;
