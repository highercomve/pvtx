const std = @import("std");
const mem = @import("std").mem;
const fs = @import("../utils/fs.zig");
const glober = @import("./glober.zig");
const models = @import("./models.zig");
const target = @import("builtin").target;

const Allocator = mem.Allocator;
const codecs = std.base64.standard_no_pad;

pub const JSON = std.json.Parsed(std.json.Value);
pub const SignedBy = std.ArrayList([]const u8);
pub const create_new_state = "empty";

const empty_state = "{\"#spec\": \"pantavisor-service-system@1\"}";
const ProtectedJson = std.json.Parsed(std.json.Value);

pub const Signatures = struct {
    allocator: Allocator,
    includes: std.json.Value,
    excludes: std.json.Value,
    parts: std.json.Value,
    protected_json: ?ProtectedJson = null,

    const Self = @This();
    pub fn init(allocator: Allocator) !*Self {
        var signatures = try allocator.create(Self);
        signatures.includes = std.json.Value{ .object = std.json.ObjectMap.init(allocator) };
        signatures.excludes = std.json.Value{ .object = std.json.ObjectMap.init(allocator) };
        signatures.parts = std.json.Value{ .object = std.json.ObjectMap.init(allocator) };

        signatures.allocator = allocator;

        return signatures;
    }

    pub fn deinit(self: *Self) void {
        var parts_iterator = self.parts.object.iterator();
        while (parts_iterator.next()) |parts| {
            parts.value_ptr.object.deinit();
        }

        var includes_iterator = self.includes.object.iterator();
        while (includes_iterator.next()) |item| {
            // self.allocator.free(item.key_ptr.*);
            item.value_ptr.array.deinit();
        }

        var excludes_iterator = self.excludes.object.iterator();
        while (excludes_iterator.next()) |item| {
            // self.allocator.free(item.key_ptr.*);
            item.value_ptr.array.deinit();
        }

        // if (self.protected_json) |protected_json| {
        //     protected_json.deinit();
        // }

        self.parts.object.deinit();
        self.includes.object.deinit();
        self.excludes.object.deinit();
        self.allocator.destroy(self);
    }
};

pub const State = struct {
    allocator: Allocator,
    json: JSON,
    raw_json: []const u8,
    signatures: *Signatures,

    const Self = @This();
    pub fn init(
        allocator: Allocator,
        json: JSON,
        data: []const u8,
        signatures: *Signatures,
    ) !*Self {
        var state = try allocator.create(Self);
        state.json = json;
        state.raw_json = data;
        state.allocator = allocator;
        state.signatures = signatures;

        return state;
    }

    pub fn deinit(self: *Self) void {
        self.allocator.free(self.raw_json);
        self.signatures.deinit();
        self.json.deinit();
        self.allocator.destroy(self);
    }
};

pub fn getStateFromFile(allocator: Allocator, source_path: []const u8) !*State {
    if (std.mem.eql(u8, source_path, create_new_state)) {
        const state = try getStateFromBuffer(allocator, empty_state);

        return state;
    }

    var json_file: ?std.fs.File = null;
    const source_path_abs = try fs.parseUserPath(allocator, source_path);
    defer allocator.free(source_path_abs);

    if (std.mem.endsWith(u8, source_path_abs, "json")) {
        json_file = try std.fs.cwd().openFile(
            source_path_abs,
            .{ .mode = .read_only },
        );
    } else {
        const path = try std.fmt.allocPrint(
            allocator,
            "{s}/.pvr/json",
            .{source_path_abs},
        );
        defer allocator.free(path);

        json_file = try std.fs.cwd().openFile(
            path,
            .{ .mode = .read_only },
        );
    }
    const stat = try json_file.?.stat();
    const data = try json_file.?.readToEndAlloc(
        allocator,
        @as(usize, @intCast(stat.size)),
    );

    const state = try getStateFromBuffer(allocator, data);

    return state;
}

pub fn getStateFromBuffer(allocator: Allocator, data: []const u8) !*State {
    const json = std.json.parseFromSlice(
        std.json.Value,
        allocator,
        data,
        .{ .allocate = .alloc_always },
    ) catch |err| switch (err) {
        std.json.Error.SyntaxError => {
            if (target.cpu.arch != .wasm32) {
                const stderr = std.io.getStdErr();
                stderr.close();

                try stderr.writer().print("the input is not a valid json file\n{s}\n", .{data});
            }
            return err;
        },
        std.json.Error.UnexpectedEndOfInput => {
            if (target.cpu.arch != .wasm32) {
                const stderr = std.io.getStdErr();
                defer stderr.close();
                try stderr.writer().print("can't parse an empty state.json -- {}\n", .{err});
            }
            return err;
        },
        else => {
            return err;
        },
    };

    const signatures = try processSignatures(allocator, json);
    return State.init(
        allocator,
        json,
        data,
        signatures,
    );
}

pub fn processSignatures(allocator: Allocator, json: JSON) !*Signatures {
    var signatures = try Signatures.init(allocator);

    for (json.value.object.keys()) |key| {
        if (!std.mem.startsWith(u8, key, "_sigs/")) {
            continue;
        }
        const value = json.value.object.get(key);
        if (value == null) {
            continue;
        }

        const protected_base64 = value.?.object.get("protected");
        if (protected_base64 == null) {
            continue;
        }

        const decoded_size = try codecs.Decoder.calcSizeForSlice(protected_base64.?.string);
        const protected = try allocator.alloc(u8, decoded_size);
        defer allocator.free(protected);

        try codecs.Decoder.decode(protected, protected_base64.?.string);

        signatures.protected_json = try std.json.parseFromSlice(
            std.json.Value,
            allocator,
            protected,
            .{ .allocate = .alloc_always },
        );

        const pvs = signatures.protected_json.?.value.object.get("pvs");
        if (pvs == null) {
            continue;
        }

        const include = pvs.?.object.get("include");
        if (include == null) {
            continue;
        }

        for (include.?.array.items) |includedV| {
            if (signatures.includes.object.get(includedV.string)) |signed_by| {
                const new_signature = std.json.Value{
                    .string = key,
                };
                var sig = @as(*std.json.Value, @constCast(&signed_by));
                try sig.array.append(new_signature);
                try signatures.includes.object.put(includedV.string, sig.*);
            } else {
                var signed_by = std.json.Value{
                    .array = std.json.Array.init(allocator),
                };
                try signed_by.array.append(std.json.Value{ .string = key });
                try signatures.includes.object.put(includedV.string, signed_by);
            }
        }

        const exclude = pvs.?.object.get("exclude");
        if (exclude == null) {
            continue;
        }
        for (exclude.?.array.items) |excludeV| {
            if (signatures.excludes.object.get(excludeV.string)) |signed_by| {
                const new_signature = std.json.Value{
                    .string = key,
                };
                var sig = @as(*std.json.Value, @constCast(&signed_by));
                try sig.array.append(new_signature);
                try signatures.excludes.object.put(excludeV.string, sig.*);
            } else {
                var signed_by = std.json.Value{
                    .array = std.json.Array.init(allocator),
                };
                try signed_by.array.append(std.json.Value{ .string = key });
                try signatures.excludes.object.put(excludeV.string, signed_by);
            }
        }
    }

    for (json.value.object.keys()) |key| {
        if (std.mem.startsWith(u8, key, "_sigs/")) {
            continue;
        }
        if (std.mem.startsWith(u8, key, "#spec")) {
            continue;
        }
        const keys = signatures.includes.object.keys();
        for (keys) |glob| {
            if (!glober.match(key, glob)) {
                continue;
            }

            for (keys) |includeKey| {
                if (!glober.match(includeKey, glob)) {
                    continue;
                }

                const signed_by = signatures.includes.object.get(includeKey).?;

                if (signatures.parts.object.get(key)) |part| {
                    for (signed_by.array.items) |sig| {
                        var part_ptr = @as(*std.json.Value, @constCast(&part));
                        try part_ptr.object.put(
                            sig.string,
                            std.json.Value{ .bool = true },
                        );

                        try signatures.parts.object.put(key, part_ptr.*);
                    }
                } else {
                    var part = std.json.Value{ .object = std.json.ObjectMap.init(allocator) };
                    for (signed_by.array.items) |sig| {
                        try part.object.put(sig.string, std.json.Value{ .bool = true });
                    }
                    try signatures.parts.object.put(key, part);
                }
            }
        }
    }

    return signatures;
}

pub fn mergeState(
    allocator: Allocator,
    current_state: *State,
    reader: anytype,
    max_size: usize,
) !void {
    const data = try reader.readAllAlloc(allocator, max_size);
    var patch_state = try getStateFromBuffer(allocator, data);
    defer patch_state.deinit();

    return mergePatch(current_state, patch_state);
}

fn mergePatch(current_state: *State, patch_state: *State) !void {
    var patch_elements = patch_state.json.value.object;
    for (patch_elements.keys()) |part| {
        if (std.mem.startsWith(u8, part, "#spec")) {
            continue;
        }
        try removeFromState(current_state, part);
    }

    for (patch_elements.keys()) |part| {
        if (std.mem.startsWith(u8, part, "#spec")) {
            continue;
        }
        if (patch_elements.get(part)) |patch| {
            // create a copy to in order to clean the patch after merge
            const pathCopy = try std.json.parseFromValue(
                std.json.Value,
                current_state.allocator,
                patch,
                .{ .allocate = .alloc_always },
            );
            try current_state.json.value.object.put(part, pathCopy.value);
        }
    }
}

pub fn mergeStateFromBuffer(
    allocator: Allocator,
    current_state: *State,
    data: []const u8,
) !void {
    var patch_state = try getStateFromBuffer(allocator, data);
    defer patch_state.deinit();

    return mergePatch(current_state, patch_state);
}

pub fn mergeStateBuffer(
    allocator: Allocator,
    source: []const u8,
    patch: []const u8,
) ![]const u8 {
    const current_state = try getStateFromBuffer(allocator, source);
    const patch_state = try getStateFromBuffer(allocator, patch);

    try mergePatch(
        @as(*State, @ptrCast(current_state)),
        @as(*State, @ptrCast(patch_state)),
    );

    const new_json = try std.json.stringifyAlloc(
        allocator,
        current_state.json.value,
        .{ .whitespace = .minified },
    );

    return new_json;
}

pub fn removeStateBuffer(
    allocator: Allocator,
    source: []const u8,
    part: []const u8,
) ![]const u8 {
    const current_state = try getStateFromBuffer(allocator, source);

    try removeFromState(
        @as(*State, @ptrCast(current_state)),
        part,
    );

    const new_json = try std.json.stringifyAlloc(
        allocator,
        current_state.json.value,
        .{ .whitespace = .minified },
    );

    return new_json;
}

pub fn removeFromState(current_state: *State, part: []const u8) !void {
    if (std.mem.startsWith(u8, part, "_sigs/") and current_state.json.value.object.contains(part)) {
        const parts = current_state.signatures.parts.object;
        for (parts.keys(), parts.values()) |k, signed_by| {
            if (signed_by.object.contains(part) and signed_by.object.keys().len == 1) {
                const removed = current_state.json.value.object.orderedRemove(k);
                if (removed and target.cpu.arch != .wasm32) {
                    std.debug.print("deleting from state {s}\n", .{k});
                }
            }
        }
    }

    if (current_state.json.value.object.contains(part)) {
        const removed = current_state.json.value.object.orderedRemove(part);
        if (removed and target.cpu.arch != .wasm32) {
            std.debug.print("deleting from state {s}\n", .{part});
        }
    }
}

test "remove signature from state" {
    const data =
        \\{
        \\		"#spec": "pantavisor-service-system@1",
        \\		"_sigs/os.json": {
        \\			"#spec": "pvs@2",
        \\			"protected": "eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsib3MvKioiXSwiZXhjbHVkZSI6WyJvcy9zcmMuanNvbiIsIl9zaWdzL29zLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
        \\			"signature": "kUcJeh06dqNxaDoQkDv3vCjTGazPpqUz-EfBWZzL8e_MmjvoqZmi9oh95wTtYByrsq4lJgrXgc_9JL7YDSN9zHNH4RxEi_m8FXXk-dFXPeZDKrCrYnwf2TvtveO1VGrOlK-HCV3chstai52nFk5L9EMgTbb4sMlhhPiFewNvHJLUqIAclEAw0xSBTd6-XeKQbRP79KU61PfrW1UxYrRBni8euevAcSKgTh7w0hABYJ2XZXQL8FfLZ_pOJdZtASXJq8pmoWI7I8B5cGwdMantTg1Q2zS-LH5iU4UAJPv_pC-ylwggKSRnkQGL6r5D4bDdv3NEY__sNiYbiP_6yf2w1g"
        \\		},
        \\		"os/lxc.container.conf": "f75b500c48e52fce90eae08192678b20d08c484de787820ade1b89293457ec48",
        \\		"os/root.squashfs": "e31dcecc9ffccb4688f976d17c328dfbba874c6dd0e35f70a87afeef0c9e0b6c",
        \\		"os/root.squashfs.docker-digest": "b08b6cd75e61930ab1008e4f5bacf38b00c426bd6ecc48bc8b3aa756b45b7c0d",
        \\		"os/run.json": {
        \\			"#spec": "service-manifest-run@1"
        \\		},
        \\		"os/src.json": {
        \\			"#spec": "service-manifest-src@1"
        \\		}
        \\}
    ;

    const allocator = std.testing.allocator;
    const json = try std.fmt.allocPrint(allocator, "{s}", .{data});
    const state = try getStateFromBuffer(allocator, json);
    defer state.deinit();

    try removeFromState(state, "_sigs/os.json");

    const new_json = try std.json.stringifyAlloc(allocator, state.json.value, .{ .whitespace = .minified });
    defer allocator.free(new_json);

    try std.testing.expectEqualStrings("{\"#spec\":\"pantavisor-service-system@1\"}", new_json);
}

test "merge states" {
    const source =
        \\{
        \\		"#spec": "pantavisor-service-system@1",
        \\		"_sigs/os.json": {
        \\			"#spec": "pvs@2",
        \\			"protected": "eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsib3MvKioiXSwiZXhjbHVkZSI6WyJvcy9zcmMuanNvbiIsIl9zaWdzL29zLmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
        \\			"signature": "kUcJeh06dqNxaDoQkDv3vCjTGazPpqUz-EfBWZzL8e_MmjvoqZmi9oh95wTtYByrsq4lJgrXgc_9JL7YDSN9zHNH4RxEi_m8FXXk-dFXPeZDKrCrYnwf2TvtveO1VGrOlK-HCV3chstai52nFk5L9EMgTbb4sMlhhPiFewNvHJLUqIAclEAw0xSBTd6-XeKQbRP79KU61PfrW1UxYrRBni8euevAcSKgTh7w0hABYJ2XZXQL8FfLZ_pOJdZtASXJq8pmoWI7I8B5cGwdMantTg1Q2zS-LH5iU4UAJPv_pC-ylwggKSRnkQGL6r5D4bDdv3NEY__sNiYbiP_6yf2w1g"
        \\		},
        \\		"os/lxc.container.conf": "f75b500c48e52fce90eae08192678b20d08c484de787820ade1b89293457ec48",
        \\		"os/root.squashfs": "e31dcecc9ffccb4688f976d17c328dfbba874c6dd0e35f70a87afeef0c9e0b6c",
        \\		"os/root.squashfs.docker-digest": "b08b6cd75e61930ab1008e4f5bacf38b00c426bd6ecc48bc8b3aa756b45b7c0d",
        \\		"os/run.json": {
        \\			"#spec": "service-manifest-run@1"
        \\		},
        \\		"os/src.json": {
        \\			"#spec": "service-manifest-src@1"
        \\		}
        \\}
    ;
    const patch =
        \\{
        \\	"#spec": "pantavisor-service-system@1",
        \\	"_sigs/pvwificonnect.json": {
        \\	  "#spec": "pvs@2",
        \\	  "protected": "eyJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJuIjoidjRJa0w1MzBYRlkyT1V4aG9vOWVRaWZ1TmxRRk9mOGxHblowUlVGSm9aTkRRNWlHRWlxeXhNMEtpOVh5UUpVV1g2RHpXY3lSVVZPUklJS29hRS1yZlB3YUh1QmdOVDRXY2RzRjN6NTRsWGQ5TUNsdWdfejl5bEgtVFNLUGFKam9ORlAyTVdDX0p5dXUxbG5ucW5iNUdzUlQ1WFh1RlA4MTNwVDlnX25fRmFWbDNuendMUnQ5ZmhHOGs3Y1M5MlJyR2Y0R1pqQnYzUEtORnJWOHBvTmZ1aDI5WHJtcTBWc3ktVGpPNUphT2Q2bzVFcXhGZXBKTnMtS21mMUdMZjlfYS1rN0I3YUQyQWdKYy1hSk9GZ2VEVjNtUFBESXFfbmtPeG9tYXA0a3ZlR0RjbkxMcDg2T1VkUEJaLWVjZHl4ZmhRT3plcm44c2o4ODQ1bmhmWlpPRkN3IiwiZSI6IkFRQUIifSwicHZzIjp7ImluY2x1ZGUiOlsicHZ3aWZpY29ubmVjdC8qKiJdLCJleGNsdWRlIjpbInB2d2lmaWNvbm5lY3Qvc3JjLmpzb24iLCJfc2lncy9wdndpZmljb25uZWN0Lmpzb24iXX0sInR5cCI6IlBWUyIsIng1YyI6WyJNSUlEYWpDQ0FsS2dBd0lCQWdJUkFLQnNoZS9Bd1A5ZGI2dU9TSUxTdThJd0RRWUpLb1pJaHZjTkFRRUxCUUF3SERFYU1CZ0dBMVVFQXd3UlVHRnVkR0YyYVhOdmNpQkVaWFlnUTBFd0hoY05Nakl3TmpFeU1UQXdNREU0V2hjTk1qUXdPVEUwTVRBd01ERTRXakFhTVJnd0ZnWURWUVFEREE5d2RpMWtaWFpsYkc5d1pYSXRNREV3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQy9naVF2bmZSY1ZqWTVUR0dpajE1Q0orNDJWQVU1L3lVYWRuUkZRVW1oazBORG1JWVNLckxFelFxTDFmSkFsUlpmb1BOWnpKRlJVNUVnZ3Fob1Q2dDgvQm9lNEdBMVBoWngyd1hmUG5pVmQzMHdLVzZEL1AzS1VmNU5JbzlvbU9nMFUvWXhZTDhuSzY3V1dlZXFkdmtheEZQbGRlNFUvelhlbFAyRCtmOFZwV1hlZlBBdEczMStFYnlUdHhMM1pHc1ovZ1ptTUcvYzhvMFd0WHltZzErNkhiMWV1YXJSV3pMNU9NN2tsbzUzcWprU3JFVjZrazJ6NHFaL1VZdC8zOXI2VHNIdG9QWUNBbHo1b2s0V0I0TlhlWTg4TWlyK2VRN0dpWnFuaVM5NFlOeWNzdW56bzVSMDhGbjU1eDNMRitGQTdONnVmeXlQenpqbWVGOWxrNFVMQWdNQkFBR2pnYWd3Z2FVd0NRWURWUjBUQkFJd0FEQWRCZ05WSFE0RUZnUVUxZTN4WmhIUkJKejJKSkp5UjE2ZjJMRzIyR013VndZRFZSMGpCRkF3VG9BVVdZdVErcEtDNElicG1DbXZFL2o0SWVxUmdUMmhJS1FlTUJ3eEdqQVlCZ05WQkFNTUVWQmhiblJoZG1semIzSWdSR1YySUVOQmdoUmpjUVowZWlxcXRCazgwZUJqSUFTVFNjV0p5akFUQmdOVkhTVUVEREFLQmdnckJnRUZCUWNEQXpBTEJnTlZIUThFQkFNQ0I0QXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQlcwVUN6SDVtN2txNFE0Yi9qelFLM05QeTlQRjhwOWxoOEtzNWR2SElBUWJMbU1LeUZiWFFpcm5YcWVlZUhYOStNVEU2TDdtSlVaNkVab2JlMWppSjZURzNwcnE4M1dyUUhFaEJQV1k5NWFsZFBBblhCeEhPS3o5by9xYTMrZVVZWHVoTVorUFBiWWRjbVpvZTgyQTc3SlZzRzFKRmNlaURsWFZ6YjgvK2p1bWd4MFUzc1hXN2FBcjFaV2FNNFZqWkx0bGZ3Qk9KL1FDTThsdFJhUjJPejdtVGpmVTUzWlgzUThzVkFUR2VVRWhHbFN6SEhUQjQvb01nRnZ3YWhPaWZTeTJuUk1FL0F5L0NqTDMvSDIxRTBHRHAvaUJvQWdQbzcvQWhNZEU5aUYyV2d0Y0F3RVVFcHZqdldJNFlrN1libTMzUEl0VGhsN0NDTmNvNHVOTzlvPSJdfQ",
        \\	  "signature": "s96LinBfoz1fOOCkmB7S25RiMr18LoHlC8vMevV_ASF6mvyJUfytzoAVcwY8P8DDo8RdLZN191PaA7n7KOV-xufjPtMCpyr7RgNE3h8h0CxNTG4MniUu0pj1eAwp1j0DyF5GTAeyhpShlKwH-rGuFQD__N-GZkA35pOpalapbUAsH6eeyQ5JxibkeVULqM033xHstosb3rt2Bf_kdmivR2MmNHx4Xnf-fbpy7t7OuIpTCp_g10NY8iWNZIIuadJVVG_9Gzg9k5OIJqv0eqK4viU81eRSlmo-4C5ySwfDOqJ5tqF-sklaKzPRguQB_DEldF7qnlx7ooDKeNGKtBcLFQ"
        \\	},
        \\	"pvwificonnect/lxc.container.conf": "931d35bb40ea06a2eb1252c2870654084fffa427cbf4212f089150d1ee766c26",
        \\	"pvwificonnect/root.squashfs": "2426b28719be0365fef5ad3daa7c9b3ebd1c5ba40800c85dd342ba84a04d6607",
        \\	"pvwificonnect/root.squashfs.docker-digest": "4f09c02beff558e41f0b7daef69f380a33a2d7f0ab6f3114dff9593515a093d6",
        \\	"pvwificonnect/run.json": {
        \\	  "#spec": "service-manifest-run@1",
        \\	},
        \\	"pvwificonnect/src.json": {
        \\		"#spec": "service-manifest-src@1"
        \\	}
        \\}
    ;

    const allocator = std.testing.allocator;
    const source_json = try std.fmt.allocPrint(allocator, "{s}", .{source});
    const source_state = try getStateFromBuffer(allocator, source_json);
    defer source_state.deinit();

    const patch_json = try std.fmt.allocPrint(allocator, "{s}", .{patch});
    const patch_state = try getStateFromBuffer(allocator, patch_json);
    defer patch_state.deinit();

    try mergePatch(source_state, patch_state);
}
