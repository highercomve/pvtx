// state_wasm.zig
const std = @import("std");
const mg = @import("./state_mg.zig");

// Use Wasm's linear memory
var buffer: [1024 * 1024]u8 = undefined; // 1MB buffer
var fixed_buffer_allocator = std.heap.FixedBufferAllocator.init(&buffer);
const allocator = fixed_buffer_allocator.allocator();

// Helper function to copy data to WASM memory
fn copyToWasmMemory(data: []const u8) u32 {
    const ptr = fixed_buffer_allocator.allocator().alloc(u8, data.len) catch return 0;
    @memcpy(ptr, data);
    return @intFromPtr(ptr.ptr);
}

// Helper function to read from WASM memory
fn readFromWasmMemory(ptr: u32, len: u32) []const u8 {
    return @as([*]const u8, @ptrFromInt(ptr))[0..len];
}

/// WebAssembly exports must use simple numeric types
export fn mergeStates(
    src_ptr: u32,
    src_len: u32,
    patch_ptr: u32,
    patch_len: u32,
    out_len_ptr: u32,
) u32 {
    const src_data = readFromWasmMemory(src_ptr, src_len);
    const patch_data = readFromWasmMemory(patch_ptr, patch_len);

    const result = mg.mergeStateBuffer(allocator, src_data, patch_data) catch {
        const out_len = @as(*u32, @ptrFromInt(out_len_ptr));
        out_len.* = 0;
        return 0;
    };
    defer allocator.free(result);

    // Store the result length
    const out_len = @as(*u32, @ptrFromInt(out_len_ptr));
    out_len.* = @intCast(result.len);

    // Copy result to WASM memory
    return copyToWasmMemory(result);
}

export fn removeFromState(
    src_ptr: u32,
    src_len: u32,
    key_ptr: u32,
    key_len: u32,
    out_len_ptr: u32,
) u32 {
    const src_data = readFromWasmMemory(src_ptr, src_len);
    const key_data = readFromWasmMemory(key_ptr, key_len);

    const result = mg.removeStateBuffer(allocator, src_data, key_data) catch {
        const out_len = @as(*u32, @ptrFromInt(out_len_ptr));
        out_len.* = 0;
        return 0;
    };
    defer allocator.free(result);

    // Store the result length
    const out_len = @as(*u32, @ptrFromInt(out_len_ptr));
    out_len.* = @intCast(result.len);

    // Copy result to WASM memory
    return copyToWasmMemory(result);
}

export fn freeMemory(ptr: u32, len: u32) void {
    if (ptr == 0 or len == 0) return;
    const slice = @as([*]u8, @ptrFromInt(ptr))[0..len];
    allocator.free(slice);
}
