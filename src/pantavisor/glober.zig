const std = @import("std");
const mem = std.mem;

const double_star = "**";
const single_start = "*";

const GlobError = error{
    GlobCanNotBeEmpty,
};

pub fn match(s: []const u8, glob: []const u8) bool {
    if (glob.len == 0 or s.len == 0) {
        return true;
    }
    var splited_s = mem.split(u8, s, "/");
    var splited_glob = mem.split(u8, glob, "/");

    var current = splited_glob.next();
    var next: ?[]const u8 = null;
    while (splited_s.next()) |folder| {
        if (current == null) {
            break;
        }
        if (mem.eql(u8, current.?, double_star)) {
            if (next == null) {
                next = splited_glob.next();
            }
            if (next == null) {
                continue;
            }
            const next_folder = splited_s.next();
            if (next_folder == null) {
                continue;
            }
            if (mem.eql(u8, next.?, next_folder.?)) {
                current = next;
                next = null;
            }
            continue;
        }

        if (mem.eql(u8, current.?, single_start)) {
            current = splited_glob.next();
            next = null;
            continue;
        }

        if (mem.eql(u8, current.?, folder)) {
            current = splited_glob.next();
            next = null;
            continue;
        }

        return false;
    }

    return true;
}

test match {
    const testing = std.testing;

    try testing.expect(match("/asd/fgh/jkl/asd", "/**/asd"));
    try testing.expect(match("", "*"));
    try testing.expect(match("/dsa/asd", "/*/asd"));
    try testing.expect(match("/asd/asd", "/*/asd"));
    try testing.expect(!match("/aaa/bbb/ccc", "/*/*/asd"));
    try testing.expect(match("/aaa/bbb/asd", "/*/*/asd"));
    try testing.expect(match("/aaa/bbb/asd", "/aaa/**"));
    try testing.expect(match("/aaa/bbb/asd", "/aaa/*/*"));
    try testing.expect(match("/aaa/bbb/asd", "/aaa/bbb/asd"));
    try testing.expect(match("_config/ventilation_ecu_io/etc/app/ecu_io.json", "_config/ventilation_ecu_io/etc/**"));
}
