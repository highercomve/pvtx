const std = @import("std");
const Allocator = @import("std").mem.Allocator;
const JSON = @import("./state_mg.zig").JSON;
const http_client = @import("../http/client.zig");

const FetchOptions = std.http.Client.FetchOptions;
const FetchResult = std.http.Client.FetchResult;
const Uri = std.Uri;
const http = std.http;
const assert = std.debug.assert;

pub const Response = http_client.StdResponse;

const cmd_url = "http://localhost/commands";
const device_meta_url = "http://localhost/device-meta";
const user_meta_url = "http://localhost/user-meta";
const config_url = "http://localhost/config";
const steps_url = "http://localhost/steps";

const pvctrl = "/pantavisor/pv-ctrl";

socket_path: []const u8,
allocator: Allocator,

const PvClient = @This();

pub fn init(allocator: Allocator) PvClient {
    return PvClient{
        .allocator = allocator,
        .socket_path = pvctrl,
    };
}

pub fn deinit(self: *PvClient) void {
    // self.client.connection_pool.deinit(self.allocator);
    // self.client.deinit();
    // self.allocator.destroy(self);
    self.* = undefined;
}

pub fn getSteps(self: *PvClient, rev: ?[]const u8) !Response {
    var url = try self.allocator.dupe(u8, steps_url);
    defer self.allocator.free(url);

    if (rev) |r| {
        url = try std.fs.path.join(self.allocator, &[_][]const u8{
            steps_url,
            r,
        });
    }

    const response = try self.do(
        .GET,
        url,
        null,
        null,
    );

    return response;
}

pub fn deleteUserMeta(self: *PvClient, key: []const u8) !Response {
    const url = try std.fmt.allocPrint(
        self.allocator,
        "{s}/{s}",
        .{ user_meta_url, key },
    );
    defer self.allocator.free(url);

    const response = try self.do(
        .DELETE,
        url,
        null,
        null,
    );

    return response;
}

pub fn deleteDevMeta(self: *PvClient, key: []const u8) !Response {
    const url = try std.fmt.allocPrint(
        self.allocator,
        "{s}/{s}",
        .{ device_meta_url, key },
    );
    defer self.allocator.free(url);

    const response = try self.do(
        .DELETE,
        url,
        null,
        null,
    );

    return response;
}

pub fn saveUsermeta(
    self: *PvClient,
    key: []const u8,
    value: []const u8,
) !Response {
    const url = try std.fmt.allocPrint(
        self.allocator,
        "{s}/{s}",
        .{ user_meta_url, key },
    );
    defer self.allocator.free(url);

    const response = try self.do(
        .PUT,
        url,
        value,
        http_client.content_type_text,
    );

    return response;
}

pub fn saveDevmeta(
    self: *PvClient,
    key: []const u8,
    value: []const u8,
) !Response {
    const url = try std.fmt.allocPrint(
        self.allocator,
        "{s}/{s}",
        .{ device_meta_url, key },
    );
    defer self.allocator.free(url);

    const response = try self.do(
        .PUT,
        url,
        value,
        http_client.content_type_text,
    );

    return response;
}

pub fn getDevmeta(self: *PvClient) !Response {
    const response = try self.do(
        .GET,
        device_meta_url,
        null,
        null,
    );

    return response;
}

pub fn getUsermeta(self: *PvClient) !Response {
    const response = try self.do(
        .GET,
        user_meta_url,
        null,
        null,
    );

    return response;
}

pub fn getConfig(self: *PvClient) !Response {
    const response = try self.do(
        .GET,
        config_url,
        null,
        null,
    );

    return response;
}

pub fn reboot(self: *PvClient, message: ?[]const u8) !Response {
    const payload = try std.fmt.allocPrint(
        self.allocator,
        "{{\"op\":\"REBOOT_DEVICE\",\"payload\":\"{s}\"}}",
        .{message orelse ""},
    );
    defer self.allocator.free(payload);

    const response = try self.do(
        .POST,
        cmd_url,
        payload,
        null,
    );

    return response;
}

pub fn signal(self: *PvClient, s: []const u8, message: ?[]const u8) !Response {
    const payload = try std.fmt.allocPrint(
        self.allocator,
        "{{\"op\":\"{s}\",\"payload\":\"{s}\"}}",
        .{ s, message orelse "" },
    );
    defer self.allocator.free(payload);

    const response = try self.do(
        .POST,
        cmd_url,
        payload,
        null,
    );

    return response;
}

pub fn poweroff(self: *PvClient, message: ?[]const u8) !Response {
    const payload = try std.fmt.allocPrint(
        self.allocator,
        "{{\"op\":\"POWEROFF_DEVICE\",\"payload\":\"{s}\"}}",
        .{message orelse ""},
    );
    defer self.allocator.free(payload);

    const response = try self.do(
        .POST,
        cmd_url,
        payload,
        null,
    );

    return response;
}

pub fn run(self: *PvClient, rev: []const u8) !Response {
    const payload = try std.fmt.allocPrint(
        self.allocator,
        "{{\"op\":\"LOCAL_RUN\",\"payload\":\"{s}\"}}",
        .{rev},
    );
    defer self.allocator.free(payload);

    const response = try self.do(
        .POST,
        cmd_url,
        payload,
        null,
    );

    return response;
}

pub fn gc(self: *PvClient) !Response {
    const payload = try std.fmt.allocPrint(
        self.allocator,
        "{{\"op\":\"RUN_GC\",\"payload\":\"\"}}",
        .{},
    );
    defer self.allocator.free(payload);

    const response = try self.do(
        .POST,
        cmd_url,
        payload,
        null,
    );

    return response;
}

pub fn commit(self: *PvClient, rev: []const u8, state: []const u8) !Response {
    const step_url = try std.fmt.allocPrint(
        self.allocator,
        "{s}/{s}",
        .{ steps_url, rev },
    );
    defer self.allocator.free(step_url);

    const response = try self.do(
        .PUT,
        step_url,
        state,
        null,
    );

    return response;
}

pub fn do(
    self: *PvClient,
    method: http.Method,
    url: []const u8,
    payload: ?[]const u8,
    content_type: ?[]const u8,
) !Response {
    var response = Response.init(self.allocator);
    var ct: []const u8 = http_client.content_type_json;
    if (content_type) |value| {
        ct = value;
    }
    const result = try self.fetch(.{
        .method = method,
        .payload = payload,
        .location = .{
            .url = url,
        },
        .headers = http.Client.Request.Headers{
            .content_type = .{
                .override = ct,
            },
        },
        .response_storage = .{
            .dynamic = &response.body.?,
        },
    });
    response.status_code = @intFromEnum(result.status);

    return response;
}

pub fn fetch(self: *PvClient, options: FetchOptions) !FetchResult {
    var client = std.http.Client{ .allocator = self.allocator };
    defer client.deinit();

    const conn = try client.connectUnix(self.socket_path);

    const uri = switch (options.location) {
        .url => |u| try Uri.parse(u),
        .uri => |u| u,
    };
    var server_header_buffer: [16 * 1024]u8 = undefined;

    const method: http.Method = options.method orelse
        if (options.payload != null) .POST else .GET;

    var req = try client.open(method, uri, .{
        .server_header_buffer = options.server_header_buffer orelse &server_header_buffer,
        .redirect_behavior = options.redirect_behavior orelse
            if (options.payload == null) @enumFromInt(3) else .unhandled,
        .headers = options.headers,
        .extra_headers = options.extra_headers,
        .privileged_headers = options.privileged_headers,
        .keep_alive = options.keep_alive,
        .connection = conn,
    });
    defer req.deinit();

    if (options.payload) |payload| req.transfer_encoding = .{ .content_length = payload.len };

    try req.send();

    if (options.payload) |payload| try req.writeAll(payload);

    try req.finish();
    try req.wait();

    switch (options.response_storage) {
        .ignore => {
            req.response.skip = true;
        },
        .dynamic => |list| {
            const max_append_size = options.max_append_size orelse 2 * 1024 * 1024;
            try req.reader().readAllArrayList(list, max_append_size);
        },
        .static => |list| {
            const buf = b: {
                const buf = list.unusedCapacitySlice();
                if (options.max_append_size) |len| {
                    if (len < buf.len) break :b buf[0..len];
                }
                break :b buf;
            };
            list.items.len += try req.reader().readAll(buf);
        },
    }

    return .{
        .status = req.response.status,
    };
}
