const std = @import("std");
const mem = @import("std").mem;
const Allocator = mem.Allocator;

const TOTAL_BAR_WIDTH: usize = 50;
const gzip_header: [2]u8 = [2]u8{ 0x1F, 0x8B }; // gzip magic number

pub const FsError = error{
    FileWithoutData,
};

const magic_offset = 257;
const magic_length = 6;
const tar_magic = "ustar";

fn isTarFile(buffer: []const u8) bool {
    return std.mem.eql(u8, buffer[magic_offset..512][0..magic_length], tar_magic);
}

pub fn isGzipFile(filename: []const u8) bool {
    var file: ?std.fs.File = null;
    if (std.mem.eql(u8, filename, "-")) {
        file = std.io.getStdIn();
        defer file.?.close();
    } else {
        file = std.fs.cwd().openFile(filename, .{}) catch {
            return false;
        };
        defer file.?.close();
    }

    var buffer: [2]u8 = undefined;
    const read_result = file.?.read(buffer[0..]) catch {
        return false;
    };

    if (read_result != buffer.len) {
        return false;
    }

    return std.mem.eql(u8, buffer[0..], gzip_header[0..]);
}

pub fn isGzipContent(data: []const u8) bool {
    return std.mem.eql(u8, data[0..2], gzip_header[0..]);
}

pub fn isTarData(data: []const u8) bool {
    const ustar_magic = "ustar";
    const ustar_offset = 257;

    if (data.len < ustar_offset + ustar_magic.len) {
        return false;
    }

    return std.mem.eql(u8, data[ustar_offset .. ustar_offset + ustar_magic.len], ustar_magic);
}

pub fn fileExists(path: []const u8) bool {
    const stat = std.fs.cwd().statFile(path) catch {
        return false;
    };

    switch (stat.kind) {
        .file => return true,
        else => return false,
    }

    return false;
}

pub fn readFileOrStdin(allocator: Allocator, path: []const u8, max_bytes: usize) ![]u8 {
    var data: ?[]u8 = null;
    var json_file: ?std.fs.File = null;
    if (std.mem.eql(u8, path, "-")) {
        json_file = std.io.getStdIn();
        defer json_file.?.close();
        data = try json_file.?.readToEndAlloc(
            allocator,
            max_bytes,
        );
    } else {
        const source_path_abs = try parseUserPath(allocator, path);
        defer allocator.free(source_path_abs);

        json_file = try std.fs.cwd().openFile(
            source_path_abs,
            .{ .mode = .read_only },
        );
        defer json_file.?.close();

        const stat = try json_file.?.stat();
        data = try json_file.?.readToEndAlloc(
            allocator,
            @as(usize, @intCast(stat.size)),
        );
    }

    if (data == null) {
        return FsError.FileWithoutData;
    }

    return data.?;
}

pub fn parseUserPath(allocator: Allocator, path: []const u8) ![]const u8 {
    const path_resolved = try std.fs.path.resolve(allocator, &[_][]const u8{path});
    defer allocator.free(path_resolved);

    const home_user = std.process.getEnvVarOwned(allocator, "HOME") catch "";
    defer allocator.free(home_user);

    return std.mem.replaceOwned(u8, allocator, path_resolved, "~", home_user);
}

pub fn openOrCreate(allocator: Allocator, path: []const u8) !std.fs.File {
    const path_parsed = try parseUserPath(allocator, path);
    defer allocator.free(path_parsed);

    const file = std.fs.cwd().openFile(
        path_parsed,
        .{ .mode = .read_write },
    ) catch |err| switch (err) {
        error.FileNotFound => {
            return std.fs.cwd().createFile(path_parsed, .{});
        },
        else => {
            return err;
        },
    };

    return file;
}

pub fn truncateFile(allocator: Allocator, path: []const u8) !std.fs.File {
    const path_parsed = try parseUserPath(allocator, path);
    defer allocator.free(path_parsed);

    const file = std.fs.cwd().createFile(
        path_parsed,
        .{ .truncate = true },
    );

    return file;
}

pub fn openOrCreateDir(allocator: Allocator, path: []const u8) !std.fs.Dir {
    const path_parsed = try parseUserPath(allocator, path);
    defer allocator.free(path_parsed);

    const dir = std.fs.cwd().openDir(
        path_parsed,
        .{},
    ) catch |err| switch (err) {
        error.FileNotFound => {
            try std.fs.makeDirAbsolute(path_parsed);
            return std.fs.cwd().openDir(
                path_parsed,
                .{},
            );
        },
        else => {
            return err;
        },
    };

    return dir;
}

fn formatSize(allocator: Allocator, size: u64) ![]const u8 {
    const KB: u64 = 1024;
    const MB: u64 = KB * 1024;
    const GB: u64 = MB * 1024;

    if (size >= GB) {
        const gigabytes = size / GB;
        const remainder = size % GB;
        const fractional = (remainder * 100) / GB;
        return try std.fmt.allocPrint(allocator, "{d}.{d} GB", .{ gigabytes, fractional });
    } else if (size >= MB) {
        const megabytes = size / MB;
        const remainder = size % MB;
        const fractional = (remainder * 100) / MB;
        return try std.fmt.allocPrint(allocator, "{d}.{d} MB", .{ megabytes, fractional });
    } else if (size >= KB) {
        const kilobytes = size / KB;
        const remainder = size % KB;
        const fractional = (remainder * 100) / KB;
        return try std.fmt.allocPrint(allocator, "{d}.{d} KB", .{ kilobytes, fractional });
    } else {
        return try std.fmt.allocPrint(allocator, "{d} Bytes", .{size});
    }
}

pub const ProgressBarError = error{
    FileNotFound,
    CantRemoveStatus,
};
pub const FileStatus = struct {
    name: []const u8,
    size: usize,
    completed: usize,
};
pub const FileStatuses = std.StringHashMap(FileStatus);
pub const ProgressBar = struct {
    allocator: Allocator,
    mutex: std.Thread.Mutex,
    statuses: FileStatuses,
    lastUpdate: i64,

    const Self = @This();
    pub fn init(allocator: Allocator) ProgressBar {
        return ProgressBar{
            .allocator = allocator,
            .mutex = std.Thread.Mutex{},
            .statuses = FileStatuses.init(allocator),
            .lastUpdate = std.time.milliTimestamp(),
        };
    }

    pub fn deinit(self: *Self) void {
        var iterator = self.statuses.valueIterator();
        while (iterator.next()) |status| {
            self.allocator.free(status.name);
            // self.allocator.destroy(&status.completed);
            // self.allocator.destroy(&status.size);
        }
        self.statuses.deinit();
    }

    pub fn addStatus(self: *Self, name: []const u8, size: usize) !void {
        self.mutex.lock();
        defer self.mutex.unlock();

        try self.statuses.put(name, FileStatus{
            .name = name,
            .size = size,
            .completed = 0,
        });
    }

    pub fn removeStatus(self: *Self, name: []const u8) !void {
        self.mutex.lock();
        defer self.mutex.unlock();

        if (!self.statuses.remove(name)) {
            return ProgressBarError.CantRemoveStatus;
        }
    }

    pub fn updateStatusCompleted(self: *Self, name: []const u8, completed: usize) !void {
        self.mutex.lock();
        defer self.mutex.unlock();

        var status = self.statuses.getPtr(name);
        if (status) |_| {
            status.?.completed = completed;
        } else {
            return ProgressBarError.FileNotFound;
        }
    }

    pub fn print(self: *Self) !void {
        self.mutex.lock();
        defer self.mutex.unlock();

        const now = std.time.milliTimestamp();
        const diff = now - self.lastUpdate;
        if (diff < 42) {
            return;
        }

        var valueIterator = self.statuses.valueIterator();
        const stdout_file = std.io.getStdOut();
        var stdout = stdout_file.writer();

        while (valueIterator.next()) |s| {
            try printProgressBar(s, &stdout);
            try stdout.writeAll("\n"); // Move cursor down 1 line
        }
        try stdout.print("\x1b[{d}A", .{self.statuses.count()});

        self.lastUpdate = std.time.milliTimestamp();
    }

    pub fn printFinish(self: *Self) !void {
        const stdout_file = std.io.getStdOut();
        var stdout = stdout_file.writer();
        var valueIterator = self.statuses.valueIterator();

        while (valueIterator.next()) |s| {
            try printProgressBar(s, &stdout);
            try stdout.writeAll("\n"); // Move cursor down 1 line
        }
    }
};

fn printProgressBar(status: *const FileStatus, stdout: *std.fs.File.Writer) !void {
    const percentage: usize = @divTrunc(status.*.completed * 100, status.*.size);
    const numEquals: usize = @divTrunc(percentage * TOTAL_BAR_WIDTH, 100);

    // Build the progress bar string
    var bar: [TOTAL_BAR_WIDTH]u8 = undefined;
    var i: usize = 0;
    while (i < numEquals and i < TOTAL_BAR_WIDTH) : (i += 1) {
        bar[i] = '='; // Completed part
    }
    while (i < TOTAL_BAR_WIDTH) : (i += 1) {
        bar[i] = ' '; // Remaining part
    }

    // Print the progress bar
    try stdout.print("\r{s} [{s}] {d}%", .{ status.*.name, bar, percentage });
}

pub fn readAndWrite(
    file_name: []const u8,
    in: anytype,
    out: std.fs.File.Writer,
    progress_bar: ?*ProgressBar,
) !void {
    var count: usize = 0;
    var buffer: [4096]u8 = undefined;

    while (in.unread_bytes.* > 0) {
        const buf = buffer[0..@min(buffer.len, in.unread_bytes.*)];

        try in.parent_reader.readNoEof(buf);
        try out.writeAll(buf);
        in.unread_bytes.* -= buf.len;

        if (progress_bar) |bar| {
            count = count + buf.len;
            try bar.updateStatusCompleted(file_name, count);
            try bar.print();
        }
    }
}

pub fn readWrite(
    in: anytype,
    out: anytype,
) !void {
    var buffer: [4096]u8 = undefined;

    while (true) {
        const read = try in.readAtLeast(
            &buffer,
            buffer.len,
        );

        if (read > 0) {
            try out.writeAll(&buffer);
        }
    }
}

pub fn mimeTypeFromFilename(filename: []const u8) []const u8 {
    const ext = std.fs.path.extension(filename);

    if (std.mem.endsWith(u8, ext, ".css")) {
        return "text/css";
    } else if (std.mem.endsWith(u8, ext, ".jpg")) {
        return "image/jpeg";
    } else if (std.mem.endsWith(u8, ext, ".jpeg")) {
        return "image/jpeg";
    } else if (std.mem.endsWith(u8, ext, ".js")) {
        return "application/javascript";
    } else if (std.mem.endsWith(u8, ext, ".json")) {
        return "application/json; charset=utf-8";
    } else if (std.mem.endsWith(u8, ext, ".png")) {
        return "image/png";
    } else if (std.mem.endsWith(u8, ext, ".svg")) {
        return "image/svg+xml";
    } else if (std.mem.endsWith(u8, ext, ".html")) {
        return "text/html; charset=utf-8";
    } else {
        return "application/octet-stream";
    }
}

pub fn readFileAndTrimNewlines(allocator: Allocator, filename: []const u8) ![]u8 {
    // Open the file
    const file = try std.fs.cwd().openFile(filename, .{});
    defer file.close();

    // Get the file size
    const file_size = try file.getEndPos();

    // Allocate memory for the file content
    var content = try allocator.alloc(u8, @as(usize, @intCast(file_size)));
    errdefer allocator.free(content);

    // Read the entire file
    const bytes_read = try file.readAll(content);

    // Trim trailing newline characters
    const trimmed_content = std.mem.trimRight(u8, content[0..bytes_read], "\r\n");

    // Resize the slice to match the trimmed content
    if (trimmed_content.len < bytes_read) {
        content = try allocator.realloc(content, trimmed_content.len);
    }

    return content[0..trimmed_content.len];
}
