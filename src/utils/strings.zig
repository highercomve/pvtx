const std = @import("std");

pub fn joinStrings(allocator: std.mem.Allocator, strings: [][]const u8, delimiter: []const u8) ![]u8 {
    var total_len: usize = 0;
    for (strings) |s| {
        total_len += s.len;
    }
    total_len += delimiter.len * (strings.len - 1);

    var buffer = try allocator.alloc(u8, total_len);

    var offset: usize = 0;
    var i: i32 = 0;
    for (strings) |s| {
        std.mem.copyForwards(u8, buffer[offset .. offset + s.len], s);
        offset += s.len;
        if (i < strings.len - 1) {
            std.mem.copyForwards(u8, buffer[offset .. offset + delimiter.len], delimiter);
            offset += delimiter.len;
        }
        i = i + 1;
    }

    return buffer;
}

const trueValues = [_][]const u8{ "true", "1", "yes", "on" };
const falseValues = [_][]const u8{ "false", "0", "no", "off" };

pub fn parseBool(s: []const u8) bool {
    for (trueValues) |val| {
        if (std.mem.eql(u8, s, val)) {
            return true;
        }
    }

    for (falseValues) |val| {
        if (std.mem.eql(u8, s, val)) {
            return false;
        }
    }

    return false;
}
