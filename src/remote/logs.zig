const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const strings = @import("../utils/strings.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,
    source: ?[]const u8 = "/",
    rev: ?[]const u8 = "current",
    tail: ?bool = false,
    follow: ?bool = false,
    sources: ?bool = false,
    number: ?[]const u8 = "+0",

    pub const shorts = .{
        .help = .h,
        .source = .s,
        .rev = .r,
        .tail = .t,
        .follow = .f,
        .number = .n,
    };

    pub const messages = .{
        .help = "how to use logs",
        .source = "source of the logs",
        .rev = "revision of the logs",
        .tail = "tail your files",
        .follow = "follow will stream your logs",
        .sources = "show available logs sources",
        .number = "number of lines to skip on tail",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "logs",
description: []const u8 = "logs print",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.args.sources.?) {
        const url = try std.fmt.allocPrint(
            allocator,
            "{s}/cgi-bin/pvtx/logs-parts?rev={s}",
            .{ pargs.?.url.?, opt.args.rev.? },
        );
        defer allocator.free(url);
        std.debug.print("{s}\n", .{url});

        client.fetch(
            allocator,
            .GET,
            url,
            null,
            client.content_type_json,
            writer,
            true,
        ) catch |err| {
            std.debug.print("error on request to {s} -- {}\n", .{ url, err });
        };

        return;
    }

    if (opt.args.follow) |_| {
        opt.args.tail = true;
    }

    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/logs?follow={}&n={s}&tail={}&source={s}&rev={s}",
        .{
            pargs.?.url.?,
            opt.args.follow.?,
            opt.args.number.?,
            opt.args.tail.?,
            opt.args.source.?,
            opt.args.rev.?,
        },
    );
    defer allocator.free(url);

    client.get_stream(
        allocator,
        url,
        "text/plain",
        writer,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}
