const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
    };

    pub const messages = .{
        .help = "how to use add",
        .pretty = "pretty format for json output",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "add",
description: []const u8 = "add package to current transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "<FILE_PATH> ...",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.positional_args.items.len == 0) {
        try opt.print_help(writer);
        return;
    }

    for (opt.positional_args.items) |filename| {
        const file_content = try readFile(allocator, filename);
        defer allocator.free(file_content);

        const url = try std.fmt.allocPrint(
            allocator,
            "{s}/cgi-bin/pvtx/add",
            .{pargs.?.url.?},
        );
        defer allocator.free(url);

        const m = try std.fmt.allocPrint(
            allocator,
            "Uploading platform {s}...\n",
            .{filename},
        );
        defer allocator.free(m);
        try writer.writeAll(m);

        const response = try client.fetch_std(
            allocator,
            .PUT,
            url,
            file_content,
            client.content_type_form,
        );
        defer response.deinit();

        if (response.body) |_| {
            const message = try std.fmt.allocPrint(
                allocator,
                "Platform added {s}\n",
                .{filename},
            );
            defer allocator.free(m);
            try writer.writeAll(message);
        } else {
            const rbody = try std.fmt.allocPrint(
                allocator,
                "{d}",
                .{response.status_code},
            );
            try writer.writeAll(rbody);
            break;
        }
    }
}

fn readFile(allocator: Allocator, path: []const u8) ![]const u8 {
    const file = try std.fs.cwd().openFile(path, .{});
    defer file.close();

    const file_stat = try file.stat();

    // convert file_size into usize to be able of work in 32bit and 64bit archs

    const buffer = try allocator.alloc(u8, @as(usize, @intCast(file_stat.size)));
    const read_count = try file.read(buffer);
    std.debug.assert(read_count == file_stat.size);

    return buffer;
}
