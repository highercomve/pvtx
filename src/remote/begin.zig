const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const not_empty = "";
const empty = "?empty=true";

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,
    empty: ?bool = false,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
    };

    pub const messages = .{
        .help = "how to use add",
        .pretty = "pretty format for json output",
        .empty = "start new empty transaction",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "begin",
description: []const u8 = "begin transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    const extra = getExtra(opt.args.empty.?);

    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/begin{s}",
        .{
            pargs.?.url.?,
            extra,
        },
    );
    defer allocator.free(url);

    client.fetch(
        allocator,
        .POST,
        url,
        null,
        client.content_type_json,
        writer,
        opt.args.pretty.?,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}

fn getExtra(e: bool) []const u8 {
    if (e) {
        return empty;
    } else {
        return not_empty;
    }
}
