const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const strings = @import("../utils/strings.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    help: ?bool,

    pub const shorts = .{
        .help = .h,
    };

    pub const messages = .{
        .help = "This will run pantavisor garbage collection",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "gc",
description: []const u8 = "run pantavisor garbage collection",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/gc",
        .{
            pargs.?.url.?,
        },
    );
    defer allocator.free(url);

    client.fetch(
        allocator,
        .POST,
        url,
        null,
        client.content_type_json,
        writer,
        false,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}
