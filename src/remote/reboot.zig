const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    message: ?[]const u8 = null,
    pretty: ?bool = true,
    help: ?bool,

    pub const shorts = .{
        .help = .h,
        .message = .m,
    };

    pub const messages = .{
        .help = "how to use reboot",
        .message = "reboot message",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "reboot",
description: []const u8 = "reboot device using pvcontrol",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/reboot",
        .{pargs.?.url.?},
    );
    defer allocator.free(url);

    client.fetch(
        allocator,
        .POST,
        url,
        null,
        client.content_type_json,
        writer,
        opt.args.pretty.?,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}
