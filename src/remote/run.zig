const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
    };

    pub const messages = .{
        .help = "how to use run",
        .pretty = "pretty format for json output",
    };
};
// Command definiton
const Command = @This();

name: []const u8 = "run",
description: []const u8 = "run revision using pvcontrol",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.positional_args.items.len == 0) {
        try opt.print_help(writer);
        return;
    }

    if (opt.positional_args.items.len > 1) {
        try opt.print_help(writer);
        return;
    }

    const rev = opt.positional_args.items[0];
    defer allocator.free(rev);

    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/run?rev={s}",
        .{ pargs.?.url.?, rev },
    );
    defer allocator.free(url);

    client.fetch(
        allocator,
        .POST,
        url,
        null,
        client.content_type_json,
        writer,
        opt.args.pretty.?,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}
