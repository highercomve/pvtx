const std = @import("std");
const mem = @import("std").mem;
const args_parser = @import("../cli/argsparser.zig");
const cmd = @import("../cli/command.zig");
const client = @import("../http/client.zig");
const strings = @import("../utils/strings.zig");
const RemoteArguments = @import("../commands/remote.zig").RemoteArguments;

const Allocator = std.mem.Allocator;
const Arguments = struct {
    pretty: ?bool = false,
    help: ?bool,

    pub const shorts = .{
        .help = .h,
        .pretty = .p,
    };

    pub const messages = .{
        .help = "how to use remove",
        .pretty = "pretty format for json output",
    };
};

// Command definiton
const Command = @This();
name: []const u8 = "remove",
description: []const u8 = "remove a package from current transaction",

pub fn init() cmd.Command {
    const command = Command{};
    return cmd.Command.init(&command, &RemoteArguments{});
}

pub fn run(
    _: *const Command,
    allocator: Allocator,
    pargs: ?*const RemoteArguments,
    args: [][:0]u8,
    writer: std.fs.File.Writer,
) anyerror!void {
    var opt = args_parser.parse(
        allocator,
        Arguments,
        args,
        "<part> ...",
        "0.0.1",
    ) catch {
        return;
    };
    defer opt.deinit();

    if (opt.positional_args.items.len == 0) {
        try opt.print_help(writer);
        return;
    }

    const parts = try strings.joinStrings(
        allocator,
        opt.positional_args.items,
        ",",
    );
    const url = try std.fmt.allocPrint(
        allocator,
        "{s}/cgi-bin/pvtx/remove?part={s}",
        .{ pargs.?.url.?, parts },
    );
    defer allocator.free(url);

    client.fetch(
        allocator,
        .POST,
        url,
        null,
        client.content_type_json,
        writer,
        opt.args.pretty.?,
    ) catch |err| {
        std.debug.print("error on request to {s} -- {}\n", .{ url, err });
    };
}
