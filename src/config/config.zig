const std = @import("std");
const builtin = @import("builtin");

pub fn getGpaConfig() std.heap.GeneralPurposeAllocatorConfig {
    if (builtin.mode == .Debug) {
        return std.heap.GeneralPurposeAllocatorConfig{
            .safety = true,
            .never_unmap = false,
            .retain_metadata = false,
            .verbose_log = false,
            .thread_safe = true,
        };
    } else {
        return std.heap.GeneralPurposeAllocatorConfig{
            .safety = false,
            .never_unmap = false,
            .retain_metadata = false,
            .verbose_log = false,
            .thread_safe = true,
        };
    }
}

pub fn is_debug(allocator: std.mem.Allocator) bool {
    const debug_env = std.process.getEnvVarOwned(allocator, "DEBUG") catch "false";
    return std.mem.eql(u8, debug_env, "true");
}
