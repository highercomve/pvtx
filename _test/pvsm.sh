#!/bin/sh

set -e

cmd=$0
dir=$(sh -c "cd $(dirname $cmd); pwd")

result_file="$(mktemp)"
expected_file="$(mktemp)"
diffing="$(mktemp)"

export PVCONTROL=$dir/mocks/pvcontrol
export PVTXDIR=$dir/localpvtx

[ -d $PVTXDIR ] || mkdir -p $PVTXDIR
[ -d $dir/results ] || mkdir -p $dir/results

alias pvsm="$dir/../zig-out/bin/pvsm-local"

testSignatureRemoval() {
  pvsm abort >/dev/null || true
  # echo "pvsm begin "
  pvsm begin -s $dir/mocks/data/state.json >/dev/null
  # echo "pvsm remove "
  pvsm remove _sigs/awconnect.json >/dev/null

  cat ~/.pvsm/state.json | jq -S >$expected_file
  cat $dir/expected/state_after_remove.json | jq -S >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testSignatureRemoval2() {
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state.json  >/dev/null
  pvsm remove awconnect >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_remove.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testSignatureRemoval3() {
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state.json  >/dev/null
  pvsm remove _config/awconnect >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_remove.json | jq -S >$expected_file

  assertNotEquals "$expected_file" "$result_file"
}

testRemovalConfigPkg() {
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state-1.json >/dev/null
  pvsm remove _sigs/nginx-config.json >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_remove_config_pkg.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testPackageUpdate() {
  cd $dir

  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state.json >/dev/null

  cd $dir/mocks/data/awconnect/
  tar c json | gzip | pvsm add -o $dir/localobjects >/dev/null

  cd $dir
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_adding_existing.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testAddPackageFromTar() {
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state-empty.json >/dev/null
  pvsm add -o $dir/localobjects $dir/mocks/data/watchdog_pinger.tar >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat $dir/localrevisions/000/.pvr/json | jq -S >$result_file
  cat $dir/expected/test_add_from_tar.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}


testAddNewPackage() {
  cd $dir/mocks/data/vaultwarden/
  tar c json | gzip >/tmp/vaultwarden.tar.gz
  cd $dir
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state.json >/dev/null
  pvsm add -o $dir/localobjects /tmp/vaultwarden.tar.gz >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_new_package.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testAddNewPackageFromCat() {
  cd $dir
  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/state.json >/dev/null

  cat $dir/mocks/data/pvwificonnect.tgz | pvsm add -o $dir/localobjects - >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_pvwificonnect.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testUpdateBsp() {
  cd $dir/mocks/data/bsp_update/
  tar c json | gzip >/tmp/bsp_update.tar.gz

  cd $dir

  pvsm abort >/dev/null || true
  pvsm begin -s $dir/mocks/data/bsp_init/json >/dev/null

  pvsm add -o $dir/localobjects /tmp/bsp_update.tar.gz >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_bsp_update.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testUpdateBspWithGroups() {
  cd $dir/mocks/data/bsp_update/
  tar c json | gzip >/tmp/bsp_update.tar.gz

  cd $dir

  pvsm abort >/dev/null || true

  pvsm begin -s $dir/mocks/data/bsp_init_groups/json >/dev/null

  pvsm add -o $dir/localobjects /tmp/bsp_update.tar.gz >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_bsp_update.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testInstallFromTgz() {
  cd $dir/mocks/data/os/
  tar c json | gzip >/tmp/os.tar.gz

  pvsm abort >/dev/null || true

  pvsm begin -s $dir/mocks/data/state-empty.json >/dev/null

  pvsm add -o $dir/localobjects /tmp/os.tar.gz >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_os_install.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testTwoPackageSigningSameFiles() {
  cd $dir/mocks/data/config_b/
  tar c json | gzip >/tmp/config_b.tar.gz

  cd $dir
  pvsm abort >/dev/null || true

  pvsm begin -s $dir/mocks/data/config_a/json >/dev/null

  pvsm add -o $dir/localobjects /tmp/config_b.tar.gz >/dev/null
  pvsm remove _sigs/config_a.json >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_install_config_b_remove_config_a.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

testTwoPackageSigningSameFilesWithGlobs() {
  cd $dir/mocks/data/config_b_glob/
  tar c json | gzip >/tmp/config_b_glob.tar.gz

  cd $dir
  pvsm abort >/dev/null || true

  pvsm begin -s $dir/mocks/data/config_a_glob/json >/dev/null

  pvsm add -o $dir/localobjects /tmp/config_b_glob.tar.gz >/dev/null
  pvsm remove _sigs/config_a.json >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_after_install_config_b_remove_config_a_with_glob.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}


testRemovalOfSignedConfig() {
  cd $dir/mocks/data/pvws_with_config/
  tar c json | gzip >/tmp/pvws_with_config.tar.gz

  cd $dir/mocks/data/pvws_without_config/
  tar c json | gzip >/tmp/pvws_without_config.tar.gz

  cd $dir

  pvsm abort >/dev/null || true

  pvsm begin -s $dir/mocks/data/state-empty.json >/dev/null
  pvsm add -o $dir/localobjects /tmp/pvws_with_config.tar.gz >/dev/null

  pvsm show | jq -S >$result_file
  cat $dir/expected/state_pvws_with_config.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"

  pvsm add -o $dir/localobjects /tmp/pvws_without_config.tar.gz >/dev/null
  rm -rf $dir/localrevisions/000
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/000 >/dev/null

  cat ~/.pvsm/state.json | jq -S >$result_file
  cat $dir/expected/state_pvws_without_config.json | jq -S >$expected_file

  diff $expected_file $result_file >$diffing
  assertEquals "" "$(cat $diffing)"
}

testDeploy()  {
  pvsm abort || true

  rm -rf $dir/localrevisions/001 || true
  cp -r $dir/localrevisions/0 $dir/localrevisions/001

  pvsm begin -s $dir/localrevisions/001/.pvr/json >/dev/null

  pvsm unpack -o $dir/localobjects $dir/mocks/data/pvwificonnect.tgz >/dev/null

  pvsm show | jq -S >$expected_file
  pvsm deploy -o $dir/localobjects -d $dir/localrevisions/001 >/dev/null

  cat $dir/localrevisions/001/.pvr/json | jq -S >$result_file

  diff $expected_file $result_file >$diffing

  assertEquals "" "$(cat $diffing)"
}

# testQueueNew()  {
#   pvsm queue new $dir/localqueue $dir/localobjects >/dev/null
#   status=$(pvsm queue status | head -n 6)
#   result=$(cat <<EOF
# queue folder: $dir/localqueue
# objects folder: $dir/localobjects

# queue:

# objects:
# EOF
# )
#   assertEquals "$result" "$status"
# }

# testQueueActions()  {
#   pvsm queue new $dir/localqueue $dir/localobjects >/dev/null
#   pvsm queue remove "nginx"
#   pvsm queue remove "_config/nginx"
#   pvsm queue remove "_sigs/nginx-config.json"
#   pvsm queue unpack $dir/mocks/data/pvwificonnect.tgz

#   result=$(pvsm queue status | head -n 9)
#   expected=$(cat <<EOF
# queue folder: $dir/localqueue
# objects folder: $dir/localobjects

# queue:
# 001__nginx.remove
# 002___config%2Fnginx.remove
# 003___sigs%2Fnginx-config.json.remove
# 004__pvwificonnect/json
# EOF
# )

#   assertEquals "$expected" "$result"
# }

# testQueueProcess()  {
#   pvsm abort || true

#   pvsm queue new $dir/localqueue $dir/localobjects >/dev/null
#   pvsm queue remove "_sigs/nginx.json"

#   # Download directly from gitlab
#   # pvwificonnect-v1.0.0-g82ca1d7.arm32v6.tgz
#   curl --silent --location "https://gitlab.com/pantacor/pvwificonnect/-/package_files/129853072/download" | pvsm queue unpack -

#   pvsm begin $dir/mocks/data/state-1.json >/dev/null

#   pvsm queue process >/dev/null

#   pvsm show | jq -S >$result_file
#   cat $dir/expected/test_queue_process.json | jq -S >$expected_file

#   diff $expected_file $result_file >$diffing

#   assertEquals "" "$(cat $diffing)"
# }

# testDeployDifferentFolder()  {
#   pvsm abort || true

#   pvsm queue new $dir/localqueue $dir/localobjects >/dev/null

#   pvsm queue unpack $dir/mocks/data/pvwificonnect.tgz >/dev/null

#   PVCONTROL_MOCK_STATE=../../localrevisions/0/.pvr/json pvsm begin >/dev/null

#   pvsm queue process $dir/localqueue >/dev/null
#   pvsm deploy $dir/localrevisions/1 $dir/localobjects >/dev/null

#   src=$(sha256sum $dir/localrevisions/1/.pv/pv-initrd.img | awk '{print $1}')
#   obj_link=$(sha256sum $dir/localrevisions/1/bsp/pantavisor | awk '{print $1}')
#   assertEquals "pv-initrd.img is a symbol link of $src" "$obj_link" "$src"

#   src=$(sha256sum $dir/localrevisions/1/.pv/pv-kernel.img | awk '{print $1}')
#   obj_link=$(sha256sum $dir/localrevisions/1/bsp/kernel.img | awk '{print $1}')
#   assertEquals "pv-kernel.img is a symbol link of $src" "$obj_link" "$src"

#   rm -rf $dir/localrevisions/1 >/dev/null
# }

# testProcessQueueWithoutbegin()  {
#   pvsm queue new $dir/localqueue $dir/localobjects >/dev/null
#   pvsm queue unpack $dir/mocks/data/pvwificonnect.tgz >/dev/null
#   pvsm queue process $dir/localrevisions/0/ >/dev/null
#   pvsm show | jq -S >$expected_file
#   pvsm deploy $dir/localrevisions/1 >/dev/null

#   cat $dir/localrevisions/1/.pvr/json | jq -S >$result_file

#   diff $expected_file $result_file >$diffing

#   assertEquals "" "$(cat $diffing)"

#   assertEquals "{\"ObjectsDir\": \"../../localobjects\"}" "$(cat $dir/localrevisions/1/.pvr/config)"
# }

# testLocalTransaction()  {
#   pvsm abort >/dev/null || true

#   pvsm begin $dir/localrevisions/0/ $dir/localobjects >/dev/null

#   pvsm remove pvwificonnect >/dev/null
#   if ! [ -f $dir/results/pvr-sdk.tgz ]; then
#     curl -s -L -o $dir/results/pvr-sdk.tgz "https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/package_files/130809443/download"
#   fi
#   pvsm add -o $dir/localobjects $dir/results/pvr-sdk.tgz >/dev/null
#   pvsm show | jq -S >$result_file
#   cat $dir/expected/test_local_transaction.json | jq -S >$expected_file

#   diff $expected_file $result_file >$diffing

#   assertEquals "" "$(cat $diffing)"

#   message="$(pvsm deploy)"
#   err=$?
#   if [ $err -ne 0 ]; then
#     assertEquals "ERROR: this is a local transaction, only can be deployed with pvsm deploy" "$message"
#   fi
# }

# testProcessManualQueue()  {
#   mkdir -p $dir/localqueue
#   mkdir -p $dir/localobjects

#   touch $dir/localqueue/001__pvwificonnect.remove
#   cp $dir/results/pvr-sdk.tgz $dir/localqueue/002__pvr-sdk.tgz

#   pvsm queue process $dir/localrevisions/0 $dir/localqueue $dir/localobjects >/dev/null
#   pvsm show | jq -S >$expected_file
#   pvsm deploy $dir/localrevisions/1 >/dev/null

#   cat $dir/localrevisions/1/.pvr/json | jq -S >$result_file

#   diff $expected_file $result_file >$diffing

#   assertEquals "" "$(cat $diffing)"
# }

setUp() {
  rm -rf $dir/localqueue >/dev/null
  rm -rf $dir/pvsm >/dev/null
}

#Load shUnit2.
. $dir/lib/shunit2
