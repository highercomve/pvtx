# pvsm: Pantavisor State Manager

This package is a binary to interact with the pvsm api inside pvr-sdk.

## Download and install

You can download the latest artifacts from

https://gitlab.com/highercomve/pvsm/-/packages/

Inside the tar gzip file you will found the pvsm binary

## Project idea

Right pvsm is just a cli to manage to talk to pvtx API, but the idea of the project is to be a full replace of pvr-sdk/pvtx

That is why pvsm has 3 components:

- Local: This component will be the cli to do actions on device.
- Remote: This component will be the cli that will talk with the API server to execute actions on device.
- Serve: simple http server that works as interface between the remote cli and the local commands.

pvr-sdk have the same components inside, they don't run in single binary nor in a single subproject. every piece is a different piece of code, with pvsm the idea is to merge everything in one src code.

## Parts implemented

### Local

- abort: erase the current transaction
- begin: start a new transaction
- remove: remove a part of the current transaction
- add: add a pvexport tar/tgz or a plain json into the current transaction. Objects are going to be put in the objects folder
- commit: creates the current transaction in the trails folder and links the .pv parts
- deploy: same as commit
- show: prints the current state of the transaction
- status: prints the status of a revision
- steps: prints the current running step or any step
- run_rev: runs a revision
- reboot: reboot the system
- get_config: prints a json with: usermeta, devicemeta and pantavisor config
- unpack: same as add

### CLI

all pvr-sdk api action are supported. the cli will work perfectly with the pvr-sdk container without the need of having a pvsm container.

- abort
- begin
- remove
- add
- commit
- deploy
- show
- status
- steps
- run_rev
- reboot
- get_config
- unpack

### Server

- POST "/cgi-bin/pvtx/begin"
- POST "/cgi-bin/pvtx/abort"
- PUT "/cgi-bin/pvtx/add"
- POST "/cgi-bin/pvtx/remove"
- PUT "/cgi-bin/pvtx/remove"
- GET "/cgi-bin/pvtx/show"
- POST "/cgi-bin/pvtx/deploy"
- POST "/cgi-bin/pvtx/commit"
- POST "/cgi-bin/pvtx/run"
- GET "/cgi-bin/pvtx/steps"
- GET "/cgi-bin/pvtx/status"
- GET "/cgi-bin/pvtx/get-config"
- POST "/cgi-bin/pvtx/reboot"

## Usage

```
pvsm local -h
 USAGE:
     pvsm local [SUBCOMMANDS]

 OPTIONS:
    - h help
    - folder, f folder where objects are saved
    - objets, o transaction folder where the transaction is saved

 SUBCOMMANDS:
   - [abort]: abort transaction
   - [begin]: begin transaction
   - [remove]: remove a package from current transaction
   - [add]: add package to current transaction
   - [commit]: commit current transaction using pvcontrol
   - [deploy]: deploy current transaction directly to filesystem
   - [show]: show transaction json state
   - [status]: show revision status
   - [steps]: show device steps
   - [run]: run revision using pvcontrol
   - [reboot]: reboot device using pvcontrol
   - [config]: config current transaction using pvcontrol
   - [unpack]: unpack pvexport and move objects to objects and process json in to transaction

 Use help of any subcommands to get more information

```

```
pvsm remote -h
 USAGE:
     pvsm remote [SUBCOMMANDS]
 
 OPTIONS:
    - help            cli help
    - url,u           device url, example: http://192.168.217.2:12369 (required)

 SUBCOMMANDS:
   - [abort]: abort transaction
   - [begin]: begin transaction
   - [remove]: remove a package from current transaction
   - [add]: add package to current transaction
   - [commit]: commit current transaction using pvcontrol
   - [deploy]: deploy current transaction
   - [show]: show transaction json state
   - [status]: show revision status
   - [steps]: show device steps
   - [run]: run revision using pvcontrol
   - [reboot]: reboot device using pvcontrol
   - [config]: config current transaction using pvcontrol
   - [logs]: logs print

 Use help of any subcommands to get more information
```

```
pvsm serve -h
 USAGE:
     serve [OPTIONS] pvsm server

 OPTIONS:
	-h, --help                        
	    --ip STRING                   server exposed ip address(default: 0.0.0.0)
	-p, --port INTEGER                server export port(default: 12369)
	-t, --theards INTEGER             numbers of theard to use(default: 1)
	-s, --transaction STRING          transaction folder path(default: ~/.pvsm)
	-o, --objects STRING              objects folder path(default: /storage/objects)
	-r, --trails STRING               trails folder path(default: /storage/trails)
	-c, --current STRING              current trail folder path(default: /storage/trails/current)
	    --files_folder STRING         (default: /app)
```
