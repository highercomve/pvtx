#!/bin/sh

set -e

running_dir=$(cd $(dirname $0); pwd)
clone_device="https://pvr.pantahub.com/pantahub-ci/rpi64_5_10_y_pvwc_connman_latest/105"

zig build -freference-trace -Dbinary=local

if ! [ -f "$running_dir/_test/pvr" ]; then
  cd $running_dir/gomodules/pvr/
  go build -o $running_dir/_test/pvr .
  cd $running_dir
fi

if ! [ -d "$running_dir/_test/localrevisions/0" ]; then
  mkdir -p $running_dir/_test/localrevisions
  rm -rf $running_dir/_test/localobjects >/dev/null 2>&1 || true
  mkdir -p $running_dir/_test/localobjects
  $running_dir/_test/pvr clone -o $running_dir/_test/localobjects "$clone_device#bsp,_sigs/bsp.json,os,_sigs/os.json,device.json,_hostconfig" $running_dir/_test/localrevisions/0
fi

for file in _test/*.sh; do
  sh $file -- $@
done
