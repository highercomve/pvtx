FROM --platform=$BUILDPLATFORM alpine:3.18 AS zig

RUN apk update && apk add --no-cache grep curl git tar xz upx

WORKDIR /tmp
RUN curl -L https://ziglang.org/download/0.13.0/zig-linux-x86_64-0.13.0.tar.xz | tar xJf - -C ./
RUN mv zig-linux-x86_64-0.13.0/zig /usr/bin && \
  mv zig-linux-x86_64-0.13.0/lib/* /usr/lib && \
  rm -rf zig-linux-x86_64-0.13.0

FROM --platform=$BUILDPLATFORM zig AS builder

ARG PLATFORM

WORKDIR /app
COPY . /app/

RUN /app/make_package.sh ${PLATFORM} true -Dbinary=all && rm -rf /app/zig-out/bin

FROM builder

WORKDIR /build
COPY --from=builder ./app/zig-out/* /build

RUN ls /build