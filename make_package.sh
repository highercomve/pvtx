#!/bin/sh
set -e

dir=$(sh -c "cd $(dirname $0) && pwd")
platform=${1:-$TARGET_PLATFORM}
VERSION=$(grep -oP '(?<=\.version = ")[^"]*' build.zig.zon)
BUILD_VERSION=$(git describe --tags --always)

if [ -z "$platform" ] || [ "$platform" == "" ] || [ "$platform" == "all" ]; then
  # platform="linux/amd64,linux/arm64,linux/arm/v5,linux/riscv64"
  platform="linux/amd64,linux/arm64,linux/arm/v5,linux/riscv64,windows/amd64,windows/arm64,darwin/arm64,darwin/amd64"
fi

docker_to_ci() {
  case "$1" in
  "linux/arm/v5"*)
    echo "linux-arm32v5"
    ;;
  "linux/arm/v6"*)
    echo "linux-arm32v6"
    ;;
  "linux/arm/v7"*)
    echo "linux-arm32v7"
    ;;
  "linux/arm64"*)
    echo "linux-arm64v8"
    ;;
  "linux/arm"*)
    echo "linux-arm32v6"
    ;;
  "linux/386"*)
    echo "linux-386"
    ;;
  "linux/amd64"*)
    echo "linux-amd64"
    ;;
  "linux/mips"*)
    echo "linux-mips"
    ;;
  "linux/mipsle"*)
    echo "linux-mipsle"
    ;;
  "linux/mips64"*)
    echo "linux-mips64"
    ;;
  "linux/mips64le"*)
    echo "linux-mips64le"
    ;;
  "linux/riscv64"*)
    echo "linux-riscv64"
    ;;
  "darwin/arm64"*)
    echo "darwin-arm64"
    ;;
  "darwin/amd64"*)
    echo "darwin-amd64"
    ;;
  "windows/amd64"*)
    echo "windows-amd64"
    ;;
  "windows/arm64"*)
    echo "windows-arm64"
    ;;
  "windows/386"*)
    echo "windows-x86"
    ;;
  esac
}

for p in $(echo $platform | tr "," "\n"); do
  ARCH=$(docker_to_ci $p)
  rm -f zig-out/bin/*
  $dir/build.sh $p $2 -Dbinary=all

  for bin in zig-out/bin/*; do
    package_name=$(basename "${bin%.*}")
    bin_name=$(basename $bin)
    NAME="${package_name}.v${VERSION}-${BUILD_VERSION}.${ARCH}.tar.gz"
    tar -czf zig-out/$NAME -C zig-out/bin $bin_name
  done
done
